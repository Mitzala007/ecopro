<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer_address extends Model
{
    use SoftDeletes;
    protected $table="customer_address";
    protected $fillable = [
        'id','addressname','first_name','last_name','company','region','address','phone','customer_id'
    ];

    public function Customer()
    {
        return $this->belongsTo('App\Customer', 'id');
    }
}
