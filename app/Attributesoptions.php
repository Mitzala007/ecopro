<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attributesoptions extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'attribute_id','attribute_name','name','price','product_id',
    ];

    public function Productattribute()
    {
        return $this->belongsTo('App\Productattributes', 'id');
    }

    public function Product()
    {
        return $this->belongsTo('App\Products', 'id');
    }
}
