<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Groups extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'name','accessright','status',
    ];
    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';

    public static $status = [
        self::STATUS_ACTIVE => 'Active',
        self::STATUS_INACTIVE => 'In Active',
    ];

    /*For access right*/

    /*FOR DISPLAY*/
    public static $access = [
        self::ACCESS_Subadmin_Management => 'Access Subadmin Management',
        self::ACCESS_Customer_Management => 'Access Customer Management',
        self::ACCESS_Banner_Management => 'Access Banner Management',
        self::ACCESS_City_Management => 'Access City Management',
        self::ACCESS_Category_Management => 'Access Category Management',
        self::ACCESS_Coupon_Management => 'Access Coupon Management',
        self::ACCESS_Product_Management => 'Access Product Management',
        self::ACCESS_Saloon_Management => 'Access Saloon Management',
    ];

    /*FOR DATABASE*/
    const ACCESS_Subadmin_Management = 'Access Subadmin Management';
    const ACCESS_Customer_Management = 'Access Customer Management';
    const ACCESS_Banner_Management = 'Access Banner Management';
    const ACCESS_City_Management = 'Access City Management';
    const ACCESS_Category_Management = 'Access Category Management';
    const ACCESS_Coupon_Management = 'Access Coupon Management';
    const ACCESS_Product_Management = 'Access Product Management';
    const ACCESS_Saloon_Management = 'Access Saloon Management';

}
