<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Productimages extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'productid','image','option_id','displayorder',
    ];

    public function Product()
    {
        return $this->belongsTo('App\Products', 'id');
    }

    public function Relatedproduct()
    {
        return $this->belongsTo('App\Relatedproducts', 'productid');
    }
}
