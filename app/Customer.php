<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    //use SoftDeletes;
    protected $fillable = [
        'id','username', 'email','recommend_email', 'password','gender','city','description','birthdate','phone','displayorder','status','newsletter','term_agree',
    ];
    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'in-active';

    public static $status = [
        self::STATUS_ACTIVE => 'active',
        self::STATUS_INACTIVE => 'in-active',
    ];

    const GENDER_MALE = 'Male';
    const GENDER_FEMALE = 'Female';

    public static $gender = [
        self::GENDER_MALE => 'Male',
        self::GENDER_FEMALE => 'Female',
    ];
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = encrypt($password);
    }

    public function Customeraddress()
    {
        return $this->hasMany('App\Customer_address', 'customer_id');
    }

    

    public static function boot()
    {
        static::deleted(function($model) {
            foreach ($model->Customeraddress as $address)
                $address->delete();
        });
        parent::boot();
    }


    const Att_52 = '52';
    const Att_105 = '105';
    const Att_212 = '212';
    const Att_530 = '530';


    public static $att_points  = [
        self::Att_52 => '52',
        self::Att_105 => '105',
        self::Att_212 => '212',
        self::Att_530 => '530',
    ];
}
