<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inventory extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'productid','name','full_name','quantity',
    ];

    public function Product()
    {
        return $this->belongsTo('App\Products', 'id');
    }
}
