<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Products extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'user_id','category','name', 'description','displayorder','is_featured','status','sku','price','shipping',
        'discountprice','stock','inventory','weight','brand','shortdescription','arrivaldate','is_featured','total_sale',
    ];
    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'in-active';

    public static $status = [
        self::STATUS_ACTIVE => 'Active',
        self::STATUS_INACTIVE => 'In Active',
    ];


    const STOCK_IN = 'In-Stock';
    const STOCK_OUT = 'Out-Of-Stock';
    const STOCK_PRE = 'Pre-Order';
    const STOCK_HIDDEN = 'Hidden';

    public static $stock = [
        self::STOCK_IN => 'In-Stock',
        self::STOCK_OUT => 'Out-Of-Stock',
        self::STOCK_PRE => 'Pre-Order',
        self::STOCK_HIDDEN => 'Hidden',
    ];
    const SHIP_FREE = 'Free shipping';
    const SHIP_STORE = 'Pick up at store';
    //const SHIP_HOT = 'Hot';

    public static $ship = [
        self::SHIP_FREE => 'Free shipping',
        self::SHIP_STORE => 'Pick up at store',
        //self::SHIP_HOT => 'Hot',
    ];

    public function Productimage()
    {
        return $this->hasMany('App\Productimages', 'productid');
    }

    public function Productattribute()
    {
        return $this->hasMany('App\Productattributes', 'productid');
    }

    public function Attributeoptions()
    {
        return $this->hasMany('App\Attributeoptions', 'product_id');
    }

    public function Inventory()
    {
        return $this->hasMany('App\Inventory', 'productid');
    }

    public function Favorites()
    {
        return $this->hasMany('App\Favorites', 'product_id')->where('type','3');
    }

    public static function boot()
    {
        static::deleted(function($model) {
            foreach ($model->Productimage as $productimage)
                $productimage->delete();
            foreach ($model->Favorites as $favorite)
                $favorite->delete();
        });
        parent::boot();
    }
}
