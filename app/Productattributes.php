<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Productattributes extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'productid','name'
    ];

    public function Product()
    {
        return $this->belongsTo('App\Products', 'productid');
    }

    public function Attributeoptions()
    {
        return $this->hasMany('App\Attributeoptions', 'attribute_id');
    }

    public static function boot()
    {
        static::deleted(function($model) {
            foreach ($model->Attributeoptions as $attribute_options)
                $attribute_options->delete();
        });
        parent::boot();
    }
}
