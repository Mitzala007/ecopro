<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coupon extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'cpn_number', 'start_date', 'end_date','disc_type','amount','cupon_use','member_use','status',
    ];
    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';

    public static $status = [
        self::STATUS_ACTIVE => 'Active',
        self::STATUS_INACTIVE => 'In Active',
    ];

    const DISCOUNT_PERCENTAGE = 'percentage discount';
    const DISCOUNT_FLAT = 'flat discount';

    public static $discount_type = [
        self::DISCOUNT_PERCENTAGE => 'Percentage Discount',
        self::DISCOUNT_FLAT => 'Flat Discount',
    ];
}
