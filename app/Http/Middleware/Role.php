<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Role
{
    public function handle($request, Closure $next)
    {
        if(Auth::user()->status != 'active')
        {
            return redirect('admin/logout');
        }
        return $next($request);
    }
}
