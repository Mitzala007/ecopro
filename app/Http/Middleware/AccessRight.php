<?php

namespace App\Http\Middleware;

use App\Groups;
use Closure;
use Illuminate\Support\Facades\Auth;

class AccessRight
{
    public function handle($request, Closure $next, $access)
    {
        $user_group = Auth::user()->group_id;
        if($user_group == 0)
        {
            return $next($request);
        }
        $group = Groups::where('id', $user_group)->first();
        if(!empty($group))
        {
            $right = explode(',', $group->accessright);
            $check = in_array($access, $right);
            if($check)
            {
                return $next($request);
            }
        }
        return redirect('admin/dashboard');
    }
}
