<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected $pagination=20;

    public function language($request){
        $cookie = $request->cookie('language');
        if(empty($cookie) && $cookie==""){
            $cookie = "en";
        }
        return $cookie;
    }


    public function image($photo,$path)
    {
        /* IMAGE UPLOAD VALIDATION */
        $img_ext = $photo->getClientOriginalExtension();
        if ($img_ext=="jpeg" || $img_ext=="jpg" || $img_ext=="png" || $img_ext=="bmp" || $img_ext=="gif" || $img_ext=="JPEG" || $img_ext=="JPG" || $img_ext=="PNG" || $img_ext=="BMP" || $img_ext=="GIF" ) {}
        else{
            return back()->withInput()->withErrors(['image' => 'Invalid file type.']);
        }
        /* ----------------------- */

        $root = base_path() . '/public/resource/'.$path ;
        $name = str_random(20).".".$photo->getClientOriginalExtension();
        $mimetype = $photo->getMimeType();
        $explode = explode("/",$mimetype);
        $type = $explode[0];

        if (!file_exists($root)) {
            mkdir($root, 0777, true);
        }
        $photo->move($root,$name);
        return $path = "public/resource/".$path."/".$name;
    }
}
