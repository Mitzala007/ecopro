<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;


    /*Check Role Of Login User*/
//    protected function authenticated(Request $request, $user)
//    {
//        if( $user->role == 'admin' || $user->role == 'owner' )
//        {
//            return redirect('/admin');
//        }
//        else
//        {
//            session()->flush();
//            return redirect()->back()->withErrors(array('global' => "Sorry, You Have Not Permission For Login."));
//        }
//    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $loginView = 'admin.auth.login';
    protected $redirectTo = '/admin';
    protected $redirectAfterLogout = 'admin/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
