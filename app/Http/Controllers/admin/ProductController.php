<?php

namespace App\Http\Controllers\admin;

use App\Attributesoptions;
use App\Categories;
use App\City;
use App\Inventory;
use App\Productattributes;
use App\Productimages;
use App\Products;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->middleware('accessright:Access Product Management');
    }

    public function index(Request $request)
    {
        $data['menu'] = "Product";
        $search='';
        if(Auth::user()->role == 'admin')
        {
            
            if(isset($request['search']) && $request['search'] != '')
            {
                $this->validate($request, [
                    'type' => 'required',
                ]);

                $data['product'] = Products::orWhere($request['type'], 'like', '%'.$request['search'].'%')->OrderBy('displayorder', 'DESC')->Paginate($this->pagination);

                $search=$request['search'];

            }
            else
            {
                $data['product'] = Products::with('Productimage')->OrderBy('displayorder', 'DESC')->Paginate($this->pagination);
            }

        }
        else
        {
            $user_id = Auth::user()->id;
            if(isset($request['search']) && $request['search'] != '')
            {
                $this->validate($request, [
                    'type' => 'required',
                ]);

                $data['product'] = Products::where('user_id',$user_id)->orWhere($request['type'], 'like', '%'.$request['search'].'%')->OrderBy('displayorder', 'DESC')->Paginate($this->pagination);

                $search=$request['search'];

            }
            else
            {
                $data['product'] = Products::where('user_id',$user_id)->with('Productimage')->OrderBy('displayorder', 'DESC')->Paginate($this->pagination);
            }
        }
        $data['search']=$search;
        return view('admin.product.index', $data);
    }

    public function update_status($id,$vid)
    {
        $status = Products::findOrFail($vid);
        $input['status'] = $id;
        $status->update($input);
        return '';
    }

    public function create()
    {
        $data = [];
        $data['menu'] = "Product";
            $role_category=Auth::user()->cat_id;
            $ids = explode(',',$role_category); 

            

            if($role_category==null)
            {

                $data['categories']= Categories::with('childs')->where('parent_id', '=', 0)->get();
            }
            else
            {
                 $data['categories']= Categories::with('childs')->whereIn('id',$ids)->get();
            }
        //$data['allCategories'] = Categories::pluck('name','id')->all();
        $data['selected_id'] = "";
        //$data['category'] = Categories::where('status', 'active')->pluck('name', 'id');
        $data['rproducts'] = DB::table('products')->pluck('name', 'id');
        $data['color'] = DB::table('attributesoptions')->where('attribute_id', '1')->pluck('name', 'id');
        return view('admin.product.create', $data);
    }

    public function store(Request $request)
    {
        $product = $request->all();

        $this->validate($request, [
            'category' => 'required',
            'name' => 'required',
            'sku' => 'required',
            'weight' => 'required',
            'image' => 'mimes:jpeg,jpg,bmp,png',
            'status' => 'required',
            'price' => 'required',
        ]);

        /* ADD DISPLAY ORDER */
        $count_display_order = Products::count();
        if ($count_display_order >= 0) {
            $c = stripslashes($count_display_order);
            $c = $c + 1;
        }
        $product['displayorder'] = $c;
        /*---------------------*/
        $product['user_id'] = Auth::user()->id;
        $product['shipping'] = implode($request['shipping'], ',');
        $product['inventory'] = 0;

        $product = Products::create($product);

        return redirect('admin/product/attribute/' . $product->id . '/edit');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data['menu'] = "Product";
        $data['product'] = Products::with('productimage')->findOrFail($id);
        //$data['category'] = Categories::where('status', 'active')->pluck('name', 'id')->all();

        /*category*/
         $role_category=Auth::user()->cat_id;
        $ids = explode(',',$role_category); 
        if($role_category==null)
        {

            $data['categories']= Categories::with('childs')->where('parent_id', '=', 0)->get();
        }
        else
        {
             $data['categories']= Categories::with('childs')->whereIn('id',$ids)->get();
        }
        
        
        //$data['allCategories'] = Categories::pluck('name','id')->all();

        $data['category'] = Categories::findOrFail($data['product']->category);
        $data['selected_id'] = $data['category']['id'];
         /*category end*/

           /*product */
        $data['rproducts'] = DB::table('products')->where('id', '!=', $id)->pluck('name', 'id');
        $data['shipping_selected'] = !empty($data['product']['shipping']) ? explode(",", $data['product']['shipping']) : "";
        $data['color'] = DB::table('attributesoptions')->where('attribute_id', '1')->pluck('name', 'id');
        return view('admin.product.edit', $data);

    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'category' => 'required',
            'name' => 'required',
            'sku' => 'required',
            'weight' => 'required',
            'image' => 'mimes:jpeg,jpg,bmp,png',
            'status' => 'required',
            'price' => 'required',
        ]);
        $product = Products::findOrFail($id);
        $input = $request->all();

        if ($input['inventory']<=0){
            $input['status']="Out-Of-Stock";
        }

        $input['shipping'] = implode($request['shipping'], ',');
        $product['is_featured'] = $request->is_featured;
        $product->update($input);
        return redirect('admin/product/attribute/' . $product->id . '/edit');
    }

    public function destroy($id)
    {
        $product = Products::with('productimage')->findOrFail($id);
        foreach ($product['productimage'] as $proimage) {
            if (file_exists($proimage->image)) {
                unlink($proimage->image);
            }
        }
        $product->delete();
        \Session::flash('danger', 'Product has been deleted successfully!');
        return redirect('admin/product/');
    }

    public function update_display_order(Request $request)
    {
        $count1 = $request->count;
        for ($i = 1; $i <= $count1; $i++) {
            if (isset($request["disp" . $i])) {
                $disp1 = $request["disp" . $i];
                $pid1 = $request["pid" . $i];
                $product = Products::findOrFail($pid1);
                $input['displayorder'] = $disp1;
                $product->update($input);
            }
        }
        \Session::flash('success', 'Display order Updated successfully!');
        return redirect('admin/product');
    }

    public function attribute_edit(Request $request, $id)
    {
        $data['menu'] = "Product";
        $data['product'] = Products::with('Productimage', 'Productattribute')->findOrFail($id);
        $data['rproducts'] = DB::table('products')->where('id', '!=', $id)->pluck('name', 'id');
        return view('admin.product.edit_attribute', $data);
    }

    public function attribute_update(Request $request, $id)
    {
        $product = Products::findOrFail($id);
        $input = $request->all();
        $count1 = $input['theValue1213'];

        /* REMOVED ATTRIBUTE AND OPTIONS */
        if (isset($input['remove_id1']) && $input['remove_id1'] != "") {
            $remove_image = explode(",", $input['remove_id1']);
            foreach ($remove_image as $val) {
                return $address = Productattributes::findOrFail($val);
                $address->delete();
            }
        }

        if ((isset($input['remove_id1']) && $input['remove_id1'] != "") && (isset($input['attribute_id']) && $input['attribute_id'] != "")) {
            $update_id1 = explode(",", $input['attribute_id']);
            $remove_attribute = explode(",", $input['remove_id1']);
            foreach ($update_id1 as $key=>$val) {
                if (in_array($val,$remove_attribute)){
                    unset($update_id1[$key]);
                }
            }
        }
        else if(isset($input['attribute_id']) && $input['attribute_id'] != "") {
            $update_id1 = explode(",", $input['attribute_id']);
        }

        for ($i = 1; $i <= $count1; $i++) {
            $j = $i - 1;

            if (!empty($update_id1[$j])) {
                $attribute = Productattributes::findOrFail($update_id1[$j]);
                if (!empty($attribute)){
                    $attrib['name']=strtolower($input['name'.$i]);
                    $attribute->update($attrib);

                    //if ($i==2) {
                    $input['option_remove_id' . $i];

                    /* REMOVE OPTION */
                    if (isset($input['option_remove_id'.$i]) && $input['option_remove_id'.$i] != "") {
                        $remove_options = explode(",", $input['option_remove_id'.$i]);
                        foreach ($remove_options as $val) {
                            $options = Attributesoptions::where('id',$val)->first();
                            if ($options){$options->delete();}
                        }
                    }

                    if(isset($input['opt_id'.$i]) && $input['opt_id'.$i] != "") {

                        $update_opt = explode(",", $input['opt_id' . $i]);

                    }

                    $option_count = $request['opt_hidden'.$i];
                    for ($m = 1; $m <= $option_count; $m++) {
                        $k = $m - 1;
                        if (!empty($update_opt[$k])) {

                            $option = Attributesoptions::findOrFail($update_opt[$k]);
                            $a= array();
                            $a['name'] = $input['value'.$i.'_'.$m];

                            if (false === strpos($input['price'.$i.'_'.$m], '+') && false === strpos($input['price'.$i.'_'.$m], '-')) {
                                //return 2;
                                $a['price'] = '+' . $input['price' . $i . '_' . $m];
                            }
                            else{
                                $a['price'] = $input['price' . $i . '_' . $m];
                            }
                            $option->update($a);
                        }
                        else{
                            $a= array();
                            $a['attribute_id'] = $attribute['id'];
                            $a['attribute_name'] = $attribute['name'];
                            $a['product_id'] = $product['id'];
                            $a['name'] = $input['value'.$i.'_'.$m];

                            if (false === strpos($input['price'.$i.'_'.$m], '+') && false === strpos($input['price'.$i.'_'.$m], '-')) {
                                //return 2;
                                $a['price'] = '+' . $input['price'.$i.'_'.$m];
                            }
                            else{
                                $a['price'] = $input['price'.$i.'_'.$m];
                            }
                            //return $a;
                            Attributesoptions::create($a);
                        }
                    }
                }
            }
            else{
                $attribute = array();
                $attribute['name'] = strtolower($input['name'.$i]);
                $attribute['productid'] = $product['id'];
                $attribute = Productattributes::create($attribute);
                $option_count = $request['opt_hidden'.$i];
                for($k=1 ; $k <= $option_count ; $k++) {
                    $a = array();
                    $a['attribute_id'] = $attribute['id'];
                    $a['attribute_name'] = $attribute['name'];
                    $a['product_id'] = $product['id'];
                    $a['name'] = $input['value'.$i.'_'.$k];

                    if (false === strpos($input['price'.$i.'_'.$k], '+') && false === strpos($input['price'.$i.'_'.$k], '-')) {
                        //return 2;
                        $a['price'] = '+' . $input['price'.$i.'_'.$k];
                    }
                    else{
                        $a['price'] = $input['price'.$i.'_'.$k];
                    }
                    Attributesoptions::create($a);
                }
            }
        }

        $product->update($input);

        \Session::flash('success', 'Product attribute added successfully');
        return redirect('admin/product/inventory/' . $product->id . '/edit');
    }

    public function inventory_edit(Request $request, $id)
    {
        $data['menu'] = "Product";
        $data['product'] = Products::with('Productimage', 'Productattribute')->findOrFail($id);
        $data['stock'] = Inventory::where('productid',$id)->get();
        return view('admin.product.edit_inventory', $data);
    }

    public function inventory_update(Request $request, $id)
    {
        $get_att_count  = Productattributes::where('productid',$id)->count();
        $a = "";
        $nm = "";
        for ($i=1;$i<=$get_att_count;$i++){
            $a .= $request['att_'.$i]."-";
            $att_name = Attributesoptions::where('id',$request['att_'.$i])->get();
            $nm .=  $att_name[0]->name."-";
        }
        $input['full_name'] = trim($nm,"-");
        $input['name'] = trim($a,"-");
        $input['quantity'] = $request['quantity'];
        $input['productid'] = $id;


        $get_inventory = Inventory::where('name',$input['name'])->where('productid',$id)->first();
        if ($get_inventory){
            $get_inventory->update($input);
            \Session::flash('success', 'Attribute stock updated successfully.');
        }
        else{
            Inventory::create($input);
            \Session::flash('success', 'Attribute stock added successfully.');
        }

        $product = Products::where('id',$id);
        $total_qty = Inventory::where('productid',$id)->sum('quantity');
        $ip['inventory'] = $total_qty;
        $product->update($ip);
        return redirect('admin/product/inventory/' . $id . '/edit');
    }

    public function inventory_delete(Request $request, $pid,$id)
    {
        $stock = Inventory::findOrFail($id);
        $stock->delete();
        \Session::flash('danger', 'Attribute stock has been deleted successfully!');
        return redirect('admin/product/inventory/' . $pid . '/edit');
    }

    public function images_edit(Request $request, $id)
    {
        $data['menu'] = "Product";
        $data['product'] = Products::findOrFail($id);
        $data['product_media'] = Productimages::where('productid',$id)->orderBy('displayorder','ASC')->get();
        $data['color'] = Attributesoptions::where('product_id',$data['product']['id'])->where('attribute_name','color')->pluck('name', 'id')->all();
        $data['total_media'] = Productimages::where('productid',$id)->count();
        return view('admin.product.edit_image', $data);
    }

    public function images_update(Request $request, $id)
    {
        $product = Products::findOrFail($id);
        if(isset($request['multimage'][0])){
            $multi_count = $request['mcnt'];

            for($i=0;$i<$multi_count;$i++) {
                if($photo = $request->file('multimage')[$i])
                {
                    $product_images  = Productimages::where('productid',$id)->count();
                    $display_order = $product_images+1;
                    $root = base_path().'/public/resource/Product/';
                    $name = str_random(20).".".$photo->getClientOriginalExtension();
                    if (!file_exists($root)) {
                        mkdir($root, 0777, true);
                    }
                    $image_path = "public/resource/Product/".$name;
                    $photo->move($root,$name);

                    $input1['image'] = $image_path;
                    $input1['productid'] = $id;
                    $input1['updated_at'] = Carbon::now();
                    $input1['displayorder'] = $display_order;
                    $imageupdate1 = new Productimages();
                    if (isset($input1['image'])){
                        $imageupdate1->insert($input1);
                    }
                }
            }
            return redirect()->back();
        }

        else{

            //return;
            $input = $request->all();
            $prev_ids = $product->Productimage->pluck('id')->all();
            $existing_ids = [];
            $ids = json_decode($request->input('ids'));
            foreach ($ids as $i){
                $media_id = $request->input('media_id' . $i);
                if ($media_id == 'NEW')
                {
                    $imagedata['productid'] = $product->id;
                    $imagedata['displayorder'] = !empty($input['displayorder'.$i])?$input['displayorder'.$i]: "";
                    if ($photo = $request->file('image' . $i)) {
                        $root = base_path() . '/public/resource/Product/';
                        $name = str_random(20) . "." . $photo->getClientOriginalExtension();
                        if (!file_exists($root)) {
                            mkdir($root, 0777, true);
                        }
                        $image_path = "public/resource/Product/" . $name;
                        $photo->move($root, $name);

                        $imagedata['image'] = $image_path;
                        $imagedata['option_id'] = $request['color'.$i];
                    }
                    $imagedata['updated_at'] = Carbon::now();
                    $imageupdate = new Productimages();
                    $imageupdate->insert($imagedata);
                }
                else{
                    $existing_ids[] = $media_id;
                    $product_image =  Productimages::findOrFail($media_id);
                    if (!empty($product_image)){
                        $media = [];
                        $media['displayorder'] = $input['displayorder'.$i];
                        if ($photo = $request->file('image' . $i)) {
                            $root = base_path() . '/public/resource/Product/';
                            $name = str_random(20) . "." . $photo->getClientOriginalExtension();
                            if (!file_exists($root)) {
                                mkdir($root, 0777, true);
                            }
                            $image_path = "public/resource/Product/" . $name;
                            $photo->move($root, $name);
                            $media['image'] = $image_path;
                        }
                        $media['option_id'] = $input['color'.$i];
                        $media['updated_at'] = Carbon::now();
                        $product_image->update($media);
                    }
                }
            }

            // Delete media
            foreach ($prev_ids as $id)
            {
                if (!in_array($id, $existing_ids))
                {
                    $product_media = Productimages::findOrFail($id);
                    $product_media->forceDelete();
                }
            }

            $product->update($input);
            \Session::flash('success', 'Product attribute added successfully');
            return redirect('admin/product');
        }
    }

    public static function categorylist($parent_id,$dases='',$selected=null)
    {
        if($parent_id==0){

            $dases   .= '';
        }
        else
        {
            $dases   .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ';
        }
        $parent_cats = Categories::where('parent_id', '=', $parent_id)->get();
        $selectedoption='';
        foreach($parent_cats as $cat)
        {
            if(isset($selected) && $selected !=null)
            {
                $selectedid = explode(',', $selected);
                if(in_array($cat->id,$selectedid))
                {
                    $selectedoption="selected='selected'";
                }
            }
            else
            {
                $selectedoption="";
            }
            echo '<option '.$selectedoption.'  value='.$cat->id.'>'.$dases.$cat->name.'</option>';
            ProductController::categorylist($cat->id,$dases,$selected);
        }
    }
}
