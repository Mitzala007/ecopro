<?php

namespace App\Http\Controllers\admin;

use App\Coupon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class CouponController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->middleware('accessright:Access Coupon Management');
    }

    public function index(Request $request)
    {
        $data['menu']="Coupon Code Management";
        $search='';
        if(isset($request['search']) && $request['search'] != '')
        {
            $this->validate($request, [
                'type' => 'required',
            ]);

            $data['discount'] = Coupon::orWhere($request['type'], 'like', '%'.$request['search'].'%')->Paginate($this->pagination);

            $search=$request['search'];

        }
        else
        {
       
            $data['discount']=Coupon::OrderBy('id','DESC')->Paginate($this->pagination);
        }
        $data['search']=$search;
        return view('admin.coupon.index',$data);
    }

    public function create()
    {
        $data=[];
        $data['menu']="Coupon Code Management";
        return view('admin.coupon.create',$data);
    }

    public function store(Request $request)
    {
        $discount = new Coupon($request->all());
        $this->validate($request, [
            'cpn_number' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'disc_type' => 'required',
            'amount' => 'required',
            'cupon_use' => 'required|integer',
            'member_use' => 'required|integer',
            'status' => 'required',
        ]);

        $discount->save();

        \Session::flash('success', 'Coupon has been inserted successfully!');
        return redirect('admin/coupon');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data['menu']="Coupon Code Management";
        $data['discount'] = Coupon::findOrFail($id);
        return view('admin.coupon.edit',$data);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'cpn_number' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'disc_type' => 'required',
            'amount' => 'required',
            'cupon_use' => 'required|integer',
            'member_use' => 'required|integer',
            'status' => 'required',
        ]);


        $discount = Coupon::findOrFail($id);
        $input = $request->all();
        $discount->update($input);

        \Session::flash('success', 'Coupon has been Updated successfully!');
        return redirect('admin/coupon');
    }

    public function destroy($id)
    {
        $discount = Coupon::findOrFail($id);
        $discount->delete();

        \Session::flash('danger','Coupon has been deleted successfully!');
        return redirect('admin/coupon');
    }

    public function assign(Request $request)
    {
        $discount = Coupon::findorFail($request['id']);
        $discount['status'] = "active";

        $discount->update($request->all());
        return $request['id'];
    }
    public function unassign(Request $request)
    {
        $discount = Coupon::findorFail($request['id']);
        $discount['status'] = "inactive";

        $discount->update($request->all());
        return $request['id'];
    }
}
