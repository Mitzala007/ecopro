<?php

namespace App\Http\Controllers\admin;

use App\Categories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->middleware('accessright:Access Category Management');
    }

    public function index(Request $request)
    {


        $data['menu'] = 'Category';

        $search='';
        if(isset($request['search']) && $request['search'] != '')
        {
            $this->validate($request, [
                'type' => 'required',
            ]);

            $data['categories'] = Categories::orWhere($request['type'], 'like', '%'.$request['search'].'%')->Paginate($this->pagination);

            $search=$request['search'];

        }
        else
        {
            $data['categories']= Categories::with('childs')->where('parent_id', '=', 0)->Paginate($this->pagination);
            $data['allCategories'] = Categories::pluck('name','id')->all();
        //$data['category'] = Categories::where('parent_id',0)->get();
        }
        $data['search']=$search;
        return view('admin.category.index',$data);
    }

    public function create()
    {
        $data['menu'] = 'Category';

        $data['categories']= Categories::with('childs')->where('parent_id', '=', 0)->get();
        $data['allCategories'] = Categories::pluck('name','id')->all();
        $data['selected_id'] = "";
        return view('admin.category.create',$data);

        $data['parent_cat'] = Categories::where('status','active')->orderBy('parent_id','desc')->get();
        //$data['parent_cat'] = Categories::where('parent_id', '=', 0)->get();
        return view('admin.category.create',$data);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'status' => 'required',
        ]);

        $input = $request->all();

        if(empty($request['parent_id']))
        {
            $input['parent_id'] = 0;
            $input['level'] = 0;
        }
        else
        {
            $input['parent_id'] = $request['parent_id'];
            $pid = Categories::where('id', $input['parent_id'])->first();
            $input['level'] = $pid['level']+1;
        }

        Categories::create($input);

        \Session::flash('success', 'Category has been inserted successfully!');
        return redirect('admin/category');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data['menu'] = 'Category';

        $data['categories']= Categories::with('childs')->where('parent_id', '=', 0)->get();
        $data['allCategories'] = Categories::pluck('name','id')->all();

        $data['category'] = Categories::findOrFail($id);
        $data['selected_id'] = $data['category']['parent_id'];

        return view('admin.category.edit',$data);


        $data['main_cat'] = Categories::where('status', 'active')->where('parent_id', '==', '0')->get();
        //$data['parent_cat'] = Categories::where('status', 'active')->where('id', '!=', $id)->pluck('name', 'id')->all();
        return view('admin.category.edit',$data);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'status' => 'required',
        ]);

        $category = Categories::findOrFail($id);
        $input = $request->all();

        if(empty($request['parent_id']))
        {
            $input['parent_id'] = 0;
            $input['level'] = 0;
        }
        else
        {
            $input['parent_id'] = $request['parent_id'];
            $pid = Categories::where('id', $input['parent_id'])->first();
            $input['level'] = $pid['level']+1;
        }

        $category->update($input);

        \Session::flash('success','Category has been updated successfully!');
        return redirect('admin/category');
    }

    public function destroy($id)
    {
        $category = Categories::findOrFail($id);
        $category->childs()->delete();
        $category->delete();


        \Session::flash('danger','Category has been deleted successfully!');
        return redirect()->back();
    }

    public function assign(Request $request)
    {
        $category = Categories::findorFail($request['id']);
        $category['status'] = "active";
        $category->update($request->all());
        return $request['id'];
    }

    public function unassign(Request $request)
    {
        $category = Categories::findorFail($request['id']);
        $category['status'] = "in-active";
        $category->update($request->all());
        return $request['id'];
    }

}
