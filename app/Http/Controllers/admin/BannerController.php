<?php

namespace App\Http\Controllers\admin;

use App\Banners;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BannerController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->middleware('accessright:Access Banner Management');
    }

    public function index(Request $request)
    {
        $data['menu'] = 'Banner';
        $search='';
        if(isset($request['search']) && $request['search'] != '')
        {
            $this->validate($request, [
                'type' => 'required',
            ]);

            $data['banner'] = Banners::orWhere($request['type'], 'like', '%'.$request['search'].'%')->Paginate($this->pagination);

            $search=$request['search'];

        }
        else
        {
          
       
            $data['banner']=Banners::OrderBy('id','DESC')->Paginate($this->pagination);
        }
        $data['search']=$search;
        return view('admin.banner.index',$data);
    }

    public function create()
    {
        $data['menu']="Banner";
        return view('admin.banner.create',$data);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'image' => 'required|mimes:jpeg,jpg,bmp,png',
            'link' => 'required',
            'status' => 'required',
        ]);

        $input = $request->all();

        if($photo = $request->file('image'))
        {
            $input['image'] = $this->image($photo,'Banner');
        }

        if (false === strpos($request->link, '://')) {
            $input['link'] = 'http://' . $request->link;
        }

        Banners::create($input);

        \Session::flash('success', 'Banner has been inserted successfully!');
        return redirect('admin/banner');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data['menu']="Banner";
        $data['banner'] = Banners::findorFail($id);
        return view('admin.banner.edit',$data);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'image' => 'mimes:jpeg,jpg,bmp,png',
            'link' => 'required',
            'status' => 'required',
        ]);

        $banner = Banners::findOrFail($id);
        $input = $request->all();

        if($photo = $request->file('image'))
        {
            $input['image'] = $this->image($photo,'Banner');
        }

        if (false === strpos($request->link, '://')) {
            $input['link'] = 'http://' . $request->link;
        }

        $banner->update($input);
        \Session::flash('success','Banner has been updated successfully!');
        return redirect('admin/banner');

    }

    public function destroy($id)
    {
        $banner = Banners::findOrFail($id);
        $banner->delete();
        \Session::flash('danger','Banner has been deleted successfully!');
        return redirect('admin/banner');
    }

    public function assign(Request $request)
    {
        $banner = Banners::findorFail($request['id']);
        $banner['status'] = "active";
        $banner->update($request->all());
        return $request['id'];
    }

    public function unassign(Request $request)
    {
        $banner = Banners::findorFail($request['id']);
        $banner['status'] = "in-active";
        $banner->update($request->all());
        return $request['id'];
    }
}
