<?php

namespace App\Http\Controllers\admin;

use App\Categories;
use App\Groups;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->middleware('accessright:Access Subadmin Management');
    }

    public function index()
    {
        $data=[];
        $data['menu']="User";
        $data['user'] = User::where('role','!=','admin')->get();
        return view('admin.users.index', $data);
    }

    public function create()
    {
        $data=[];
        $data['menu']="User";
        $data['cat_id'] = Categories::where('status', 'active')->where('parent_id','=', '0')->pluck('name', 'id');
        $data['dases'] = '';
        $data['groupname']= Groups::where('status','active')->pluck('name','id')->all();
        return view("admin.users.create",$data);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'category' => 'required',
            'status'=>'required',
            'image'=>'required|mimes:jpeg,bmp,png',
        ]);
        $input = $request->all();

        if($photo = $request->file('image'))
        {
            $input['image'] = $this->image($photo,'User');
        }
        if(!empty($request['category']))
        {
            $input['cat_id'] = implode(',', $request['category']);
        }
        $input['role'] = 'sub-admin';

        User::create($input);

        \Session::flash('success', 'User has been inserted successfully!');
        return redirect('admin/users');
    }

    public function show($id)
    {
       //
    }

    public function edit($id)
    {
        $data=[];
        $data['menu']="User";
        $data['user'] = User::findorFail($id);
        $data['cat_selected'] = explode(',', $data['user']['cat_id']);
        $data['cat_id'] = Categories::where('status', 'active')->where('parent_id','=', '0')->pluck('name', 'id');
        $data['groupname']= Groups::where('status','active')->pluck('name','id')->all();
        return view('admin.users.edit',$data);
    }

    public function update(Request $request,$id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id.',id',
            'status'=>'required',
            'image'=>'mimes:jpeg,bmp,png',
        ]);

        $input = $request->all();
        $user = User::findorFail($id);

         if(empty($request['password'])){
            unset($request['password']);

        }   
        if(!empty($request['category']))
        {
            $input['cat_id'] = implode(',', $request['category']);
        }
        
        if(!empty($request['image'] && $request['image'] != ""))
        {
            if(file_exists($user->image))
                unlink($user->image);
        }

        if($photo = $request->file('image'))
        {
            $input['image'] = $this->image($photo,'user');
        }

        if(!empty($request['parent_id']))
        {
            $input['cat_id'] = implode(',', $request['parent_id']);
        }
       // return $input;
        $user->update($input);
        \Session::flash('success','User has been updated successfully!');
        return redirect('admin/users');
    }

    public function destroy($id)
    {
        $appuser = User::findOrFail($id);
        $appuser->delete();
        \Session::flash('danger','User has been deleted successfully!');
        return redirect('admin/users');
    }

    public function assign(Request $request)
    {
        $appuser = User::findorFail($request['id']);
        $appuser['status'] = "active";
        $appuser->update($request->all());
        return $request['id'];
    }

    public function unassign(Request $request)
    {
        $appuser = User::findorFail($request['id']);
        $appuser['status'] = "in-active";
        $appuser->update($request->all());
        return $request['id'];
    }

//    public static function categorylist($parent_id,$dases='',$selected=null)
//    {
//        if($parent_id==0){
//
//            $dases   .= '';
//        }
//        else
//        {
//            $dases   .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ';
//        }
//        $parent_cats = Categories::where('parent_id', '=', $parent_id)->get();
//        $selectedoption='';
//        foreach($parent_cats as $cat)
//        {
//            if(isset($selected) && $selected !=null)
//            {
//                $selectedid = explode(',', $selected);
//                if(in_array($cat->id,$selectedid))
//                {
//                    $selectedoption="selected='selected'";
//                }
//            }
//            else
//            {
//                 $selectedoption="";
//            }
//            echo '<option '.$selectedoption.'  value='.$cat->id.'>'.$dases.$cat->name.'</option>';
//            UserController::categorylist($cat->id,$dases,$selected);
//        }
//    }

}
