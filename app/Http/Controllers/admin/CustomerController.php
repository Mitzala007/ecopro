<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;

use App\Customer_address;
use App\Customer;
use App\City;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class CustomerController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->middleware('accessright:Access Customer Management');
    }

    public function  index(Request $request)
    {
       
        $data['menu']="Customer";
         $search='';
        if(isset($request['search']) && $request['search'] != '')
        {
            $this->validate($request, [
                'type' => 'required',
            ]);

            $data['customer'] = Customer::orWhere($request['type'], 'like', '%'.$request['search'].'%')->Paginate($this->pagination);

            $search=$request['search'];

        }
        else
        {
       
            $data['customer']=Customer::OrderBy('id','DESC')->Paginate($this->pagination);
        }
        $data['search']=$search;
        return view('admin.customer.index',$data);
    }

    public function create()
    {
        $data=[];
        $data['mainmenu']="Customer";
        $data['menu']="Customer";
        $data['city'] = City::where('status', 'active')->pluck('name', 'id')->all();
        //dd($data['city']);
        return view('admin.customer.create',$data);
    }



    public function store(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'email' => 'required|email|unique:customers,email',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
            'status' => 'required',
            'gender' => 'required',
            'city' => 'required',
            'birthdate' => 'required',
            'phone' => 'required|Numeric',
        ]);

        $customer=Customer::create($request->all());


        $count = $request['theValue1212'];
        $address=$request->all();
        for($i=1;$i<=$count;$i++)
        {
            $address['addressname']=$address['addressname'.$i];
            $address['first_name']=$address['first_name'.$i];
            $address['last_name']=$address['last_name'.$i];
            $address['address']=$address['address'.$i];
            $address['phone']=$address['phone'.$i];
            $address['customer_id']=$customer->id;


            Customer_address::create($address);
        }
        \Session::flash('success', 'Customer details inserted successfully!');
        $url = $request->only('redirects_to');
        return redirect()->to($url['redirects_to']);
    }

    public function show($id)
    {
       
    }

    public function edit($id)
    {
        $data['mainmenu']="Customer";
        $data['menu']="Customer";
        $data['customer']=Customer::with('Customeraddress')->findorFail($id);
        $data['city'] = City::where('status', 'active')->pluck('name', 'id')->all();
        return view('admin.customer.edit',$data);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'username' => 'required',
            'email' => 'required|email|unique:customers,email,' . $id . ',id',
            'password' => 'confirmed',
            'status' => 'required',
            'gender' => 'required',
            'city' => 'required',
            'birthdate' => 'required',
            'phone' => 'required|Numeric',
        ]);
        $customer = Customer::findOrFail($id);

        if(!empty($request['password'])){
        }
        else{
            unset($request['password']);
        }

        $input = $request->all();



        $count = $input['theValue1212'];

        /* REMOVE ADDRESS */
        if (isset($input['remove_id']) && $input['remove_id'] != "") {
            $remove_image = explode(",", $input['remove_id']);
            foreach ($remove_image as $val) {
                $address = Customer_address::findOrFail($val);
                $address->forceDelete();
            }
        }
		
		if ((isset($input['remove_id']) && $input['remove_id'] != "") && (isset($input['address_id']) && $input['address_id'] != "")) {
            $update_id = explode(",", $input['address_id']);
            $remove_address = explode(",", $input['remove_id']);
            foreach ($update_id as $key=>$val) {
                if (in_array($val,$remove_address)){
                    unset($update_id[$key]);
                }
            }
        }
        else if(isset($input['address_id']) && $input['address_id'] != "") {
            $update_id = explode(",", $input['address_id']);
        }

        for ($i = 1; $i <= $count; $i++) {

            $j = $i - 1;
            if (!empty($update_id[$j])) {
                $address = Customer_address::findOrFail($update_id[$j]);
                if (!empty($address)){
                    $addr['addressname']=$input['addressname'.$i];
                    $addr['first_name']=$input['first_name'.$i];
                    $addr['last_name']=$input['last_name'.$i];
                    $addr['address']=$input['address'.$i];
                    $addr['phone']=$input['phone'.$i];
                    $address->update($addr);
                }
            }
            else{
                $addr['addressname']=$input['addressname'.$i];
                $addr['first_name']=$input['first_name'.$i];
                $addr['last_name']=$input['last_name'.$i];
                $addr['address']=$input['address'.$i];
                $addr['phone']=$input['phone'.$i];
                $addr['customer_id']=$customer->id;
                Customer_address::create($addr);
            }
        }
    
        $customer->update($input);


        \Session::flash('success', 'customer has been Updated successfully!');
        //return redirect('customer');
        $url = $request->only('redirects_to');
        return redirect()->to($url['redirects_to']);
    }
    public function destroy($id)
    {
        $customer = Customer::findOrFail($id);
        $customer->delete();
        \Session::flash('danger','customer has been deleted successfully!');
        return redirect('admin/customer');
    }

    public function assign(Request $request)
    {
        $customer = Customer::findorFail($request['id']);
        $customer['status'] = "active";
        $customer->update($request->all());
        return $request['id'];
    }

    public function unassign(Request $request)
    {
        $customer = Customer::findorFail($request['id']);
        $customer['status'] = "in-active";
        $customer->update($request->all());
        return $request['id'];
    }

    /*public function save_comment(Request $request,$id){
        if(empty($request->comment)) {
            \Session::flash('danger', 'Comment is required.');
            return back();
        }

        if(empty($request->orders)){
            \Session::flash('danger', 'Select record');
            return back();
        }

        $input['comment'] = $request->comment;
        point_history::wherein('id',$request->orders)->update($input);
        \Session::flash('success', 'Comment added successfully!');
        return redirect()->to('admin/customer/'.$id);
    }*/
}
