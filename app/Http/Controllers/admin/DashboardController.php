<?php

namespace App\Http\Controllers\admin;

use App\Categories;
use App\Customer;
use App\Http\Controllers\Controller;
use App\Products;
use App\User;
use Carbon\Carbon;
//use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['mainmenu'] = "";
        $data['menu'] = "Dashboard";
        $data['total_user'] = User::count();
        //$data['total_city'] = City::count();
        $data['total_cat'] = Categories::count();
        //$data['total_banner'] = Banners::count();
        //$data['total_coupan'] = Coupon::count();
        $data['total_product'] = Products::count();
        $data['total_customer'] = Customer::count();
        $data['recent_product']= Products::OrderBy('id', 'DESC')->take(4)->get();
        return view('admin.dashboard',$data);
    }
}
