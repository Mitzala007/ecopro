<?php

namespace App\Http\Controllers\admin;

use App\City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CityController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->middleware('accessright:Access City Management');
    }
    
    public function index(Request $request)
    {
        $data['menu']="City";
        $search='';
        if(isset($request['search']) && $request['search'] != '')
        {
            $this->validate($request, [
                'type' => 'required',
            ]);

            $data['city'] = City::orWhere($request['type'], 'like', '%'.$request['search'].'%')->Paginate($this->pagination);

            $search=$request['search'];

        }
        else
        {
            $data['city']=City::OrderBy('id','desc')->Paginate($this->pagination);
        }
        $data['search']=$search;
        return view('admin.city.index',$data);
    }
    public function create()
    {
        $data=[];
        $data['menu'] = "City";
        return view('admin.city.create',$data);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'status' => 'required',
        ]);
        $input = $request->all();
        City::create($input);

        \Session::flash('success', 'City has been inserted successfully!');
        return redirect('admin/city');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data=[];
        $data['menu']="City";
        $data['city'] = City::findOrFail($id);
        return view('admin.city.edit',$data);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'status' => 'required',
        ]);

        $input = $request->all();
        $city = City::findorFail($id);

        $city->update($input);

        \Session::flash('success','City has been updated successfully!');
        return redirect('admin/city');
    }

    public function destroy($id)
    {
        $city = City::findOrFail($id);
        $city->delete();

        \Session::flash('danger','City has been deleted successfully!');
        return redirect()->back();
    }
    public function assign(Request $request)
    {
        $city = City::findorFail($request['id']);
        $city['status'] = "active";
        $city->update($request->all());
        return $request['id'];
    }

    public function unassign(Request $request)
    {
        $city = City::findorFail($request['id']);
        $city['status'] = "in-active";
        $city->update($request->all());
        return $request['id'];
    }
}
