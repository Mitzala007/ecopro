<?php

namespace App\Http\Controllers\admin;

use App\Groups;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GroupController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->middleware('accessright:Access Subadmin Management');
    }

    public function index()
    {
        $data=[];
        $data['menu']="Group";
        $data['group'] = Groups::all();
        return view('admin.group.index', $data);
    }

    public function create()
    {
        $data=[];
        $data['menu']="Group";
        return view('admin.group.create',$data);
    }

    public function store(Request $request)
    {
        $input=$request->all();
        $this->validate($request, [
            'name' => 'required',
            'status' => 'required',
        ]);

        if(!empty($request['accessright'])) {
            $input['accessright'] = implode(',',$request['accessright']);
        }
        else{
            $input['accessright'] = $request['accessright'];
        }
        
        Groups::create($input);

        \Session::flash('success', 'Group has been inserted successfully!');
        return redirect('admin/group');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data['menu']="Group";
        $data['group'] = Groups::findOrFail($id);
        $data['accessselected']= explode(",",$data['group']['accessright']);
        $data['staff']=User::where('group_id',$id)->get();
        return view('admin.group.edit',$data);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'status' => 'required',
        ]);

        $group = Groups::findOrFail($id);

        $input = $request->all();
        if(!empty($request['accessright'])) {
            $input['accessright'] = implode(',',$request['accessright']);
        }
        else{
            $input['accessright'] = $request['accessright'];
        }

        $group->update($input);

//        $msg = "";
//        if ($group->name!=$input['name']){ $msg .= "Name".",";}
//        if ($group->status!=$input['status']){ $msg .= "Status".",";}
//
//
//
//        $msg = trim($msg,",");
//        if ($msg!="") {
//            Activity::log('Group', $group->id, 'Group '.$msg.' has been Updated by - '.Auth::user()->name);
//        }

        \Session::flash('success', 'Group has been updated successfully!');
        return redirect('admin/group');
    }

    public function destroy($id)
    {
        $group = Groups::findOrFail($id);
        $group->delete();

        //Activity::log('Group',$group->id,'Group Name -'.$group->name.' has been deleted');
       // Activity::log('Group',$id,'Group deleted by - '.Auth::user()->name);
        \Session::flash('danger', 'Group has been deleted successfully!');
        return redirect('admin/group');
    }

    public function assign(Request $request)
    {
        $user = Groups::findorFail($request['id']);
        $user['status']="active";
        $user->update($request->all());
        //Activity::log('Group',$request['id'],'Group Status change to active by - '.Auth::user()->name);
        return $request['id'];
    }

    public function unassign(Request $request)
    {
        $user = Groups::findorFail($request['id']);
        $user['status']="inactive";
        $user->update($request->all());
        //Activity::log('Group',$request['id'],'Group Status change to inactive by - '.Auth::user()->name);
        return $request['id'];
    }
}
