<?php

namespace App\Http\Controllers\admin;

use App\Business;
use App\Categories;
use App\City;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Bus;

class BusinessController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->middleware('accessright:Access Business Management');
    }

    public function index()
    {
        $data['menu'] = 'Business';
        if(Auth::user()->role == 'admin')
        {
            $data['business'] = Business::all();
        }
        else
        {
            $user_id = Auth::user()->id;
            $data['business'] = Business::where('status', 'active')->where('user_id',$user_id)->get();
        }

        return view('admin.business.index', $data);
    }

    public function create()
    {
        $data['menu'] = "Business";
        $data['city'] = City::where('status', 'active')->pluck('name', 'id')->all();
        $data['cat_id'] = Categories::where('status', 'active')->where('deleted_at', null)->pluck('name', 'id')->all();
        return view('admin.business.create', $data);
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'city_id' => 'required',
            'category' => 'required',
            'name' => 'required',
            'status' => 'required',
        ]);

        $input = $request->all();

        if(!empty($request['category']))
        {
            $input['cat_id'] = implode(',', $request['category']);
        }

        $input['user_id'] =  Auth::user()->id;

        Business::create($input);

        \Session::flash('success', 'Business has been inserted successfully!');
        return redirect('admin/business');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data['menu'] = "Business";
        $data['business'] = Business::findOrFail($id);
        $data['city'] = City::where('status', 'active')->pluck('name', 'id')->all();
        $data['cat_selected'] = explode(',', $data['business']['cat_id']);
        $data['cat_id'] = Categories::where('status', 'active')->where('deleted_at', null)->pluck('name', 'id')->all();
        return view('admin.business.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'city_id' => 'required',
            'category' => 'required',
            'name' => 'required',
            'status' => 'required',
        ]);

        $input = $request->all();

        $business = Business::findOrFail($id);

        if(!empty($request['category']))
        {
            $input['cat_id'] = implode(',', $request['category']);
        }

        $business->update($input);

        \Session::flash('success', 'Business has been updated successfully!');
        return redirect('admin/business');
    }

    public function destroy($id)
    {
        $business = Business::findOrFail($id);
        $business->delete();
        \Session::flash('danger', 'Business has been deleted successfully!');
        return redirect('admin/business');
    }

    public function assign(Request $request)
    {
        $business = Business::findorFail($request['id']);
        $business['status'] = "active";
        $business->update($request->all());
        return $request['id'];
    }

    public function unassign(Request $request)
    {
        $business = Business::findorFail($request['id']);
        $business['status'] = "in-active";
        $business->update($request->all());
        return $request['id'];
    }
}
