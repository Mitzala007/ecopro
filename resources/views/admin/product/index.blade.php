@extends('admin.layouts.app')
@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                {{$menu}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('admin/product') }}"><i class="fa fa-dashboard"></i> Product</a></li>
            </ol>
        </section>
        <section class="content">
            @include ('admin.error')
            <div id="responce" name="responce" class="alert alert-success" style="display: none">
            </div>

            <div class="box box-info">
                 <div class="box-header">
                    <div class="col-md-5">
                        {!! Form::open(['url' => url('admin/product'), 'method' => 'get', 'class' => 'form-horizontal','files'=>false]) !!}
                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            <div class="col-md-4 col-sm-5 col-xs-12" style="padding-top: 5px">
                                <select name="type" class="select2 form-control">
                                    <option value="">Please Select</option>
                                    <option value="name" @if (isset($_REQUEST['type']) && $_REQUEST['type']=='name') selected="selected" @endif>Name</option>
                                    <option value="price" @if (isset($_REQUEST['type']) && $_REQUEST['type']=='price') selected="selected" @endif>Price</option>
                                    <option value="sku" @if (isset($_REQUEST['type']) && $_REQUEST['type']=='sku') selected="selected" @endif>Sku</option>
                                    
                                    <option value="status" @if (isset($_REQUEST['type']) && $_REQUEST['type']=='status') selected="selected" @endif>Status</option>

                                </select>
                                @if ($errors->has('type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <span style="float:left; padding-top: 5px" class="col-md-4 col-sm-5 col-xs-12">
                                <input  class="form-control" type="text" @if(!empty($search)) value="{{$search}}" @else placeholder="Search" @endif name="search" id="search">
                            </span>
                            <span style="float:left; padding-top: 5px" class="col-lg-2 col-md-3 col-sm-2 col-xs-12">
                                <input type="submit" class="btn btn-info pull-right" name="submit" value="Search">
                            </span>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="col-md-7">
                        <h3 class="box-title" style="float:right;">
                            <a href="{{ url('admin/product/create') }}" ><button class="btn btn-info" type="button"><span class="fa fa-plus"></span></button></a>
                            <a href="{{ url('admin/product') }}" ><button class="btn btn-default" type="button"><span class="fa fa-refresh"></span></button></a>
                        </h3>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    {{--{{ Form::open(array('url' => array('admin/product/update_display_order'), 'method' => 'post','style'=>'display:inline')) }}--}}
                    <table id="example2" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Edit</th>
                            <th>Id</th>
                            <th>Name</th>
                            <th>SKU#</th>
                            <th>Weight</th>
                            <th>Price</th>
                            <th>Discounted Price</th>
                            <th>Display Order</th>
                            <th>Status</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $count = 0;?>
                        @foreach ($product as $key => $list)
                            <?php $count++; ?>
                            <input type="hidden" name="pid<?php echo $count;?>" id="pid<?php echo $count;?>" value="{{ $list['id'] }}" />
                            <tr>
                                <td>
                                    <div class="btn-group-horizontal">
                                        {{ Form::open(array('url' => 'admin/product/'.$list['id'].'/edit', 'method' => 'get','style'=>'display:inline')) }}
                                        <button class="btn btn-info tip" data-toggle="tooltip" title="Edit Product" data-trigger="hover" type="submit" ><i class="fa fa-edit"></i></button>
                                        {{ Form::close() }}
                                    </div>
                                </td>
                                <td>{{ $list['id'] }}</td>
                                <td>{{ $list['name'] }}</td>
                                <td>{{$list->sku}}</td>
                                <td>{{$list->weight}}</td>
                                <td>{{$list->price}}</td>
                                <td>{{$list->discountprice}}</td>
                                <td class="photo">
                                    <span style="display: none">{{$list['displayorder']}}</span>
                                    <input onKeyPress="return keycode();" size="5" type="textbox" name="disp<?php echo $count; ?>" value="{{$list['displayorder']}}" >
                                    <input onKeyPress="return keycode();" size="5" type="textbox" name="pid<?php echo $count; ?>" value="{{$list['id']}}" style="display:none" >
                                </td>
                                <td>
                                    <?php $status_selected = explode(",",$list['status']); ?>
                                    {!! Form::select('status', \App\Products::$stock, !empty($status_selected)?$status_selected:null,['class' => 'select2 select2-hidden-accessible form-control', 'style' => 'width: 70%','onchange'=> 'calltype(this.value,'.$list->id.','.$count.');']) !!}
                                </td>

                                <td>
                                    <div class="btn-group-horizontal">
                                        <span data-toggle="tooltip" title="Delete Product" data-trigger="hover">
                                            <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#myModal{{$list['id']}}"><i class="fa fa-trash"></i></button>
                                        </span>
                                    </div>

                                </td>
                            </tr>

                            <div id="myModal{{$list['id']}}" class="fade modal modal-danger" role="dialog">
                                {{ Form::open(array('url' => 'admin/product/'.$list['id'], 'method' => 'delete','style'=>'display:inline')) }}
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Delete Product</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>Are you sure you want to delete this Product?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-outline">Delete</button>
                                        </div>
                                    </div>
                                </div>
                                {{ Form::close() }}
                            </div>
                        @endforeach
                    </table>
                     <div style="text-align:right;float:right;"> @include('admin.pagination.limit_links', ['paginator' => $product])</div>

                    <input type="hidden" name="count" id="count" value="<?php echo $count;?>" />
                    {{--{{ Form::close() }}--}}
                </div>
            </div>

        </section>

    </div>
@endsection
<script src="{{ URL::asset('assets/jquery.js')}}"></script>
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/ladda/ladda-themeless.min.css')}}">
<script src="{{ URL::asset('assets/plugins/ladda/spin.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/ladda/ladda.min.js')}}"></script>
<script>Ladda.bind( 'input[type=submit]' );</script>

<script>
    function calltype(val,vid,cnt){
        if(val == '') val = 0;
        $.ajax({
            url: '{{ url('admin/product') }}/'+val +'/'+ vid,
            error:function(){
            },
            success: function(result){
            }
        });
    }
</script>


