@extends('admin.layouts.app')


@section('content')
    <div class="content-wrapper" style="min-height: 946px;">
        <section class="content-header">
            <h1>
                <h1>
                    {{ $menu }}
                    <small>Edit</small>
                </h1>

            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Traditional Ecommerce</a></li>
                <li><a href="#">{{ $menu }}</a></li>
                <li class="active">edit</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Product </h3>
                        </div>

                        {!! Form::model($product, ['url' => url('admin/traditional/product/related/' . $product->id), 'method' => 'patch', 'class' => 'form-horizontal','files'=>true]) !!}

                        <div class="box-body">

                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class=""><a href="{{url('admin/traditional/product/'. $product->id.'/edit')}}">General</a>
                                    </li>
                                    <li class=""><a href="{{url('admin/traditional/product/category/' . $product->id.'/edit')}}">Categories</a></li>
                                    <li class="active"><a href="#relatedproduct" data-toggle="tab" aria-expanded="false">Related-Products</a>
                                    </li>
                                    <li class=""><a href="{{url('admin/traditional/product/attribute/' . $product->id.'/edit')}}">Add-Attributes</a>
                                    </li>
                                    <li class=""><a href="{{url('admin/traditional/product/images/' . $product->id.'/edit')}}" >Images</a>
                                    </li>

                                </ul>
                                <div class="tab-content" style="display: block">

                                    <div class="tab-pane active" id="relatedproduct">

                                        <div class="form-group{{ $errors->has('relatedproducts') ? ' has-error' : '' }}">
                                            <label class="col-sm-2 control-label"
                                                   for="relatedproducts">Related-Products</label>

                                            <div class="col-sm-6">

                                                {{--{!! Form::select('relatedproducts[]', ['0'=>'Please Select']+['1'=>$rproducts], null, ['class' =>
                                                'form-control', 'style' => 'width: 100%','multiple']) !!}--}}
                                                {!! Form::select('relatedproducts[]',$rproducts, !empty($rproducts_selected)?$rproducts_selected:null,
                                                ['class' => 'form-control', 'style' => 'height:300px','width: 100%','multiple']) !!}
                                                @if ($errors->has('relatedproducts'))
                                                    <span class="help-block">
                   <strong>{{ $errors->first('relatedproducts') }}</strong>
                        </span>
                                                @endif
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="box-footer">
                            <a href="{{url('admin/traditional/product/category/'. $product->id.'/edit')}}">
                                <button class="btn btn-default" type="button">Back</button>
                            </a>
                            <button class="btn btn-info pull-right" type="submit">Edit</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection


