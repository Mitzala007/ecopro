@extends('admin.layouts.app')

@section('content')
    <div class="content-wrapper" style="min-height: 946px;">
        <section class="content-header">
            <h1>
                <h1>
                    {{ $menu }}
                    <small>Add</small>
                </h1>

            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('admin/product') }}">{{ $menu }}</a></li>
                <li class="active">Add</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">ADD {{$menu}}</h3>
                        </div>
                        {!! Form::open(['url' => url('admin/product'), 'class' => 'form-horizontal','files'=>true]) !!}
                        <div class="box-body">

                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#general" data-toggle="tab" aria-expanded="true">General</a></li>
                                    <li class=""><a>Add-Attributes</a></li>
                                    <li class=""><a>Attributes Stock</a></li>
                                    <li class=""><a>Images</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="general">

                                        {{--<div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">--}}
                                            {{--<label class="col-sm-1 control-label" for="category">Category<span class="text-red">*</span></label>--}}
                                            {{--<div class="col-sm-5">--}}
                                                {{--{!! Form::select('category', [''=>'Please select'] + $category, null, ['id'=>'category', 'class' => 'select2 form-control', 'style' => 'width: 100%']) !!}--}}
                                                {{--@if ($errors->has('category'))--}}
                                                    {{--<span class="help-block">--}}
                                                        {{--<strong>{{ $errors->first('category') }}</strong>--}}
                                                    {{--</span>--}}
                                                {{--@endif--}}
                                            {{--</div>--}}
                                        {{--</div>--}}

                                        <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                                            <label class="col-sm-1 control-label" for="category">Category <span class="text-red">*</span></label>
                                            <div class="col-sm-5">

                                               <select name="category"class="select2 form-control" style="width: 100%;">
                                                <option value="0">Select Category</option>
                                                
                                                     
                                                @foreach($categories as $category)
                                                         <option value="{{ $category->id }}" @if($selected_id==$category->id) selected @endif>{{ $category->name }}</option>
                                                        @if(count($category->childs))
                                                            @include('admin.category.managechild',['childs' => $category->childs])
                                                        @endif
                                                   
                                                @endforeach
                                                 
                                                        </li>
                                             
                                            </select>
                                                @if ($errors->has('category'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('category') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                            <label class="col-sm-1 control-label" for="title">Product Name<span class="text-red"> *</span></label>
                                            <div class="col-sm-5">
                                                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Product Name']) !!}
                                                @if ($errors->has('name'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                            <label class="col-sm-1 control-label" for="description">Description<span
                                                        class="text-red"></span></label>
                                            <div class="col-sm-5">
                                                {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Description','rows'=>'10','cols'=>'80', 'id'=>'summernote']) !!}
                                                @if ($errors->has('description'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('description') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('shortdescription') ? ' has-error' : '' }}">
                                            <label class="col-sm-1 control-label" for="title">Short Description</label>
                                            <div class="col-sm-5">
                                                {!! Form::text('shortdescription', null, ['class' => 'form-control', 'placeholder' => 'Product Short Description']) !!}
                                                @if ($errors->has('shortdescription'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('shortdescription') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>


                                        <div class="form-group{{ $errors->has('sku') ? ' has-error' : '' }}">
                                            <label class="col-sm-1 control-label" for="title">Product SKU #<span class="text-red"> *</span></label>
                                            <div class="col-sm-5">
                                                {!! Form::text('sku', null, ['class' => 'form-control', 'placeholder' => 'Product Sku']) !!}
                                                @if ($errors->has('sku'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('sku') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('weight') ? ' has-error' : '' }}">
                                            <label class="col-sm-1 control-label" for="title">Weight<span class="text-red"> *</span></label>
                                            <div class="col-sm-5">
                                                {!! Form::text('weight', null, ['class' => 'form-control', 'placeholder' => 'Product Weight']) !!}
                                                @if ($errors->has('weight'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('weight') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('shipping') ? ' has-error' : '' }}">
                                            <label class="col-sm-1 control-label" for="relatedproducts">Shipping Feature</label>
                                            <div class="col-sm-5">
                                                {!! Form::select('shipping[]',\App\Products::$ship, !empty($shipping_selected)?$shipping_selected:null, ['class' => 'select2 select2-hidden-accessible form-control','multiple', 'style' => 'width: 100%']) !!}
                                                    @if ($errors->has('shipping'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('shipping') }}</strong>
                                                        </span>
                                                    @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                                            <label class="col-sm-1 control-label" for="title">Price<span class="text-red"> *</span></label>
                                            <div class="col-sm-5">
                                                {!! Form::text('price', null, ['class' => 'form-control', 'placeholder' => 'Product Price']) !!}
                                                @if ($errors->has('price'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('price') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group{{ $errors->has('discountprice') ? ' has-error' : '' }}">
                                            <label class="col-sm-1 control-label" for="title">Discount Price</label>
                                            <div class="col-sm-5">
                                                {!! Form::text('discountprice', null, ['class' => 'form-control', 'placeholder' => 'Product Discountprice']) !!}
                                                @if ($errors->has('discountprice'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('discountprice') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group{{ $errors->has('inventory') ? ' has-error' : '' }}">
                                            <label class="col-sm-1 control-label" for="title">Inventory<span class="text-red"></span></label>
                                            <div class="col-sm-5">
                                                {!! Form::text('inventory', null, ['class' => 'form-control', 'placeholder' => 'Product Inventory','readonly']) !!}
                                                @if ($errors->has('inventory'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('inventory') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                            <label class="col-sm-1 control-label" for="status">Status<span class="text-red"> *</span></label>
                                            <div class="col-sm-5">
                                                {!! Form::select('status', [''=>'Please select']+ \App\Products::$stock, null, ['class' => 'select2
                                                select2-hidden-accessible form-control', 'style' => 'width: 100%','onchange'=>
                                                'calltype(this.value);']) !!}
                                                @if ($errors->has('status'))
                                                    <span class="help-block">
                                                       <strong>{{ $errors->first('status') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>


                                        <div class="form-group{{ $errors->has('arrivaldate') ? ' has-error' : '' }}"
                                             style="@if(isset($product->status) && $product->status=="Pre-Order") visibility:visible; @else display: none;@endif" id="stock_type">
                                            <label class="col-sm-1 control-label" for="title">Arrival Date</label>
                                            <div class="col-sm-5">
                                                {!! Form::text('arrivaldate', null, ['class' => 'form-control','placeholder' => 'Product arrivaldate']) !!}
                                                @if ($errors->has('arrivaldate'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('arrivaldate') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-1 control-label" for="is_featured">Featured</label>
                                            <div class="col-sm-16">
                                                <span style="margin-right: 16px"></span>{{ Form::checkbox('is_featured', '1', null,['class' => 'flat-red' ]) }}
                                            </div>
                                         </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <a href="{{ url('admin/product') }}" ><button class="btn btn-default" type="button">Back</button></a>
                            <button class="btn btn-info pull-right" type="submit">Add</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

<script>

    function calltype(val) {
        if (val == 'Pre-Order') {
            document.getElementById('stock_type').style.display = "block";
        }
        else {
            document.getElementById('stock_type').style.display = "none";
        }
    }
</script>


