    <section class="content-header">
        <br>
        @include ('admin.error')
        <div id="responce" name="responce" class="alert alert-success" style="display: none">
        </div>
        <div class="box box-info">
            <div class="box-body table-responsive">
                <table id="" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        {{--<th>Id</th>--}}
                        <th>Name</th>
                        <th>Quantity</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody id="sortable">
                    <?php $count = 0;?>
                    @foreach ($stock as $list)
                        <?php $count++; ?>
                        <tr class="ui-state-default">
                            <td>{{ $list['full_name'] }}</td>
                            <td>{{ $list['quantity'] }}</td>
                            <td>
                                <div class="btn-group-horizontal">
                                    <span data-toggle="modal" data-target="#myModal{{$list['id']}}" style="color: #680814; font-size: 25px;"><i class="fa fa-trash"></i></span>
                                </div>
                            </td>
                        </tr>

                        <div id="myModal{{$list['id']}}" class="fade modal modal-danger" role="dialog" >
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">Delete Inventory</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>Are you sure you want to delete this Inventory?</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                                        <a href="{{ url('admin/product/inventory/destroy/'.$product['id'].'/'.$list['id']) }}" data-method="delete" name="delete_item">
                                            <button type="button" class="btn btn-outline">Delete</button>
                                        </a>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                        </div>
                    @endforeach

                </table>

            </div>

        </div>

    </section>

    {!! Form::model($product, ['url' => url('admin/product/inventory/' . $product->id), 'method' => 'patch', 'class' => 'form-horizontal','files'=>true]) !!}

    <?php $count =1; ?>
    @foreach( $product['Productattribute'] as $k=>$v)
        <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
            <label class="col-sm-1 control-label" for="status">{{$v['name']}}<span class="text-red"> *</span></label>
            <div class="col-sm-4">
                <select name="att_{{$count}}" id="att_{{$count}}" class="select2 select2-hidden-accessible form-control" style ="width: 100%" required>
                    <option value="">Please select</option>
                    <?php
                    $opt = \App\Attributesoptions::where('product_id',$v['productid'])->where('attribute_id',$v['id'])->get();
                    foreach ($opt as $key => $val){
                        ?>
                        <option value="{{$val['id']}}">{{$val['name']}}</option>
                        <?php
                    }
                    ?>

                </select>
            </div>
        </div>
        <?php $count++; ?>
    @endforeach

    <div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
        <label class="col-sm-1 control-label" for="quantity">Quantity <span class="text-red">*</span></label>
        <div class="col-sm-4">
            {!! Form::text('quantity', null, ['class' => 'form-control', 'placeholder' => 'Quantity','required']) !!}
            @if ($errors->has('quantity'))
                <span class="help-block">
                <strong>{{ $errors->first('quantity') }}</strong>
            </span>
            @endif
        </div>
    </div>








