<?php $count = 1;?>
<div id="myDiv1">

    <p style="margin-right: 10px">
        <button data-toggle="modal" data-target="#myModal" class="btn btn-info pull-right" type="button">Upload multiple images</button>
    </p>
    <?php  $image_id = ""; ?>
    @if(isset($product_media) && !empty($product_media))
        @foreach ($product_media as $key => $value)
            <?php $image_id.=$value->id.",";
            ?>

                <div class="col-sm-12 media" data-id="{{ $count }}">
                <input name="media_id<?php echo $count; ?>" value="{{ $value->id }}" type="hidden" />
                <label class='col-sm-2 control-label' for='image'  style="margin-top: 20px;">Image</label>
                <div class='col-sm-1'>
                    <img id="DisplayImage<?php echo $count; ?>" src='{{ url($value->image) }}'
                         name="img<?php echo $count; ?>" width="75" style="padding-bottom:5px;">
                </div>
                <div class='col-sm-1'>
                    <select id="color<?php echo $count; ?>" name="color<?php echo $count; ?>" class="form-control" style="width: 100%;margin-top: 15px;">
                        <option value=''>Default</option>
                        @foreach($color as $k=>$v)
                            <option value="{{$k}}" @if($k==$value->option_id) selected="selected" @endif>{{$v}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-2">
                    <input type="file" name="image<?php echo $count; ?>" id="image<?php echo $count; ?>"
                           onChange="AjaxUploadImage(this,'<?php echo $count; ?>')" style="margin-top: 20px;">
                </div>
                <div class="col-sm-1">
                    <a href="javascript:;" onclick="removeEvent1212(this)">
                        <strong style="line-height: 60px;">[x]</strong></a>
                </div>

                <div class="col-sm-2">
                    <label class="col-sm-8 control-label" for="display_order">Image Sequence</label>
                    <select id="displayorder<?php echo $count; ?>" class="dp_class display-order" name="displayorder<?php echo $count; ?>" onchange="swap(this)" data-order="<?php echo $count; ?>"  style="width: 30%;height: 35px">
                        @for( $i=1;$i<=$total_media;$i++)
                            <option value="{{$i}}" @if($i==$value->displayorder) selected="selected" @endif>{{ $i }}</option>
                        @endfor
                    </select>
                </div>
            </div>

            <?php $count++; ?>
        @endforeach
    @endif



        <div id="myModal" class="fade modal modal-worning" role="dialog" >
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Upload Multiple Images</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group" style="padding: 10px !important;">
                                <input type="file" name="multimage[]" id="multimage" onChange="preview_images()" multiple>

                                <input type="hidden" name="mcnt" id="mcnt">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn pull-left" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-default">Upload</button>
                        </div>
                    </div>
                </div>
        </div>

        <script>
            function preview_images()
            {
                var total_file=document.getElementById("multimage").files.length;
                document.getElementById("mcnt").value=total_file;
            }
        </script>


</div>
<input type="hidden" name="total_count" value="{{ $count - 1 }}" />
<input type="hidden" name="ids" id="ids" value="" />
<script>

    var next_id = {{ $count }};
    var removed_order = 0;

    var ids = [];
    for (var i=1; i<= {{ $count - 1 }}; i++)
    {
        ids.push(i);
    }
    document.getElementById('ids').value=JSON.stringify(ids);
    //$("input[name=ids]").val(JSON.stringify(ids));

    function swap(select) {

        var new_order = $(select).val();
        var old_order = $(select).attr("data-order");


        $(".display-order").each(function (){
            if ($(this).val() == new_order)
                $(this).val(old_order).attr("data-order", old_order);
        });

        $(select).val(new_order).attr("data-order", new_order);
    }

    function rebuildMediaUI()
    {
        // Hide remove button if only media exists
        var media_count = $(".media").length;
        var options = "";
        for (var i=1; i<=media_count; i++)
        {
            options += "<option value='" + i + "'>" + i + "</option>";
        }

        $(".display-order").each(function (){
            var selectedOrder = $(this).attr("data-order");
            $(this).empty().append($(options));
            if (selectedOrder == 0)
            {
                $(this).val(media_count);
                $(this).attr("data-order", media_count);
            }
            else if (removed_order != 0 && selectedOrder > removed_order)
            {
                $(this).val(selectedOrder - 1);
                $(this).attr("data-order", selectedOrder - 1);
            }
            else
            {
                $(this).val(selectedOrder);
            }
        });

        // reset
        removed_order = 0;
    }

    function addEvent1212() {

//        var numi = document.getElementById('theValue1212');
//        var num = (document.getElementById("theValue1212").value - 1) + 2;
//        numi.value = num;

        var num = next_id++;
        var ni = document.getElementById('myDiv1');
        var newdiv1 = document.createElement('div');
        newdiv1.setAttribute("class", "col-sm-12 media");
        newdiv1.setAttribute("data-id", num);

        $("input[name=total_count]").val(num);


        newdiv1.innerHTML = "<input name='media_id" + num + "' value='NEW' type='hidden' />" +
                "<label class='col-sm-2 control-label' for='image' style=\"margin-top: 20px" + "\">Image</label>" +
                "<div class='col-sm-1'>" +
                    "<img id=\"DisplayImage" + num + "\"  src='http://localhost:81/Lets_Shopping/assets/dist/img/upload.png' name=\"img" + num + "\" width=\"75" + "\"  style=\"padding-bottom: 5px" + "\" >" +
                "</div>" +
                "<div class='col-sm-1'>"+
                "<select name=\"color" + num + "\" id=\"color" + num + "\" class='form-control' style='width: 100%;margin-top:15px'>"+
                "<option value=''>Default</option>"+
                        @foreach($color as $key=>$value)
                            "<option value='{{$key}}'>{{$value}}</option>"+
                        @endforeach
                "</select>"+
                "</div>"+
                "<div class='col-sm-2'>" +
                    "<input name=\"image" + num + "\" type=\"file\" id=\"image" + num + "\" onChange='AjaxUploadImage(this,\"" + num + "\")' style=\"margin-top: 20px" + "\"  />" +
                "</div>" +
                "<div class=\"col-sm-1" + "\" >" +
                    "<a href=\"javascript:;\" onclick=\"removeEvent1212(this)\">" +
                        "<strong style=\"line-height: 60px" + "\" >[x]</strong>" +
                    "</a>" +
                "</div>"+
                "<div class='col-sm-2'><label class='col-sm-8 control-label' for='is_shareable'>Image Sequence</label>" +
                "<select id=\"displayorder" + num + "\" name=\"displayorder" + num + "\"  class='form-control dp_class display-order' style='width: 30%;height:35px' onchange='swap(this)' data-order='0'> "+
                "</select></div>";
        ni.appendChild(newdiv1);
        document.getElementById("i").value = document.getElementById("i").value + 1;


        ids.push(num);
        $("input[name=ids]").val(JSON.stringify(ids));
        rebuildMediaUI();
    }

    function removeEvent1212(btn_remove) {

        var $media = $(btn_remove).closest(".media");

        removed_order = $media.find(".display-order").val();

        var index = ids.indexOf(parseInt($media.attr("data-id")));
        if (index > -1)
            ids.splice(index, 1);
        $("input[name=ids]").val(JSON.stringify(ids));

        $media.remove();
        rebuildMediaUI();
    }

    @if ($count == 1)
	    addEvent1213();
    @endif

//    function removeEvent1212(divNum,id) {
//        var remove = document.getElementById('remove_id').value;
//        if(remove==""){ remove = id; }
//        else{
//            remove = remove+","+id;
//        }
//
//        document.getElementById('remove_id').value = remove;
//        var d = document.getElementById('myDiv');
//        var olddiv = document.getElementById(divNum);
//        d.removeChild(olddiv);
//        document.getElementById("theValue1212").value = document.getElementById("theValue1212").value - 1;
//    }
</script>
<input type="hidden" value="{{ trim($image_id,",") }}" name="image_id" id="image_id">
<input type="hidden" value="" name="remove_id" id="remove_id">
<input type="hidden" value="{{ !empty($count)?$count-1:''  }}" name="theValue1212" id="theValue1212"/>
<input type="hidden" value="{{ !empty($count)?$count-1:''  }}" name="i" id="i"/>
<div class="col-sm-5">
    <input class="btn btn-info pull-right" type="button" value="Add Product Image" onClick="addEvent1212();">
</div>