@extends('admin.layouts.app')


@section('content')
    <div class="content-wrapper" style="min-height: 946px;">
        <section class="content-header">
            <h1>
                <h1>
                    {{ $menu }}
                    <small>Edit</small>
                </h1>

            </h1>
            <ol class="breadcrumb">
                <li><a href="#">{{ $menu }}</a></li>
                <li class="active">edit</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Product </h3>
                        </div>

                        {!! Form::model($product, ['url' => url('admin/product/images/' . $product->id), 'method' => 'patch', 'class' => 'form-horizontal','files'=>true]) !!}
                        <div class="box-body">
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class=""><a href="{{url('admin/product/'. $product->id.'/edit')}}">General</a></li>
                                    <li class=""><a href="{{url('admin/product/attribute/' . $product->id.'/edit')}}">Add-Attributes</a></li>
                                    <li class=""><a href="{{url('admin/product/inventory/' . $product->id.'/edit')}}">Attribute Stock</a></li>
                                    <li class="active"><a href="#images" >Images</a></li>
                                </ul>
                                <div class="tab-content" style="display: block">
                                    <div class="tab-pane active" id="images">
                                        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                                            @include('admin.product.add_images')
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <a href="{{url('admin/product/attribute/'. $product->id.'/edit')}}" ><button class="btn btn-default" type="button">Back</button></a>
                            <button class="btn btn-info pull-right" type="submit">Edit</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection


<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('editor1');
    //bootstrap WYSIHTML5 - text editor
    $(".textarea").wysihtml5();
</script>


<script>

    $("#image").fileinput({
        showUpload: false,
        showCaption: false,
        showPreview: false,
        showRemove: false,
        browseClass: "btn btn-primary btn-lg btn_new",
    });

    function AjaxUploadImage(obj, id) {
        var file = obj.files[0];
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/jpg"];
        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
            $('#previewing' + URL).attr('src', 'noimage.png');
            alert("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
            //$("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
            return false;
        } else {
            var reader = new FileReader();
            reader.onload = imageIsLoaded;
            reader.readAsDataURL(obj.files[0]);
        }

        function imageIsLoaded(e) {

            $('#DisplayImage' + id).css("display", "block");
            $('#DisplayImage' + id).attr('src', e.target.result);
            $('#DisplayImage' + id).attr('width', '75');

        };

    }


    function calltype(val) {
        if (val == 'Pre-Order') {
            document.getElementById('stock_type').style.display = "block";
        }
        else {
            document.getElementById('stock_type').style.display = "none";
        }
    }
</script>


