<div id="myDiv1">
    <?php  $attribute_id = ""; ?>
    @if(isset($product->Productattribute[0]) && !empty($product->Productattribute[0]))
        <?php $count1 = 1;
        ?>
        @foreach ($product->Productattribute as $key => $v)
            <?php $attribute_id .= $v->id . ",";
            ?>

            <div class="col-sm-12" id="Account1<?php echo $count1; ?>">

                <hr>
                <div class="form-group ">
                    <label class="col-sm-2 control-label">Name<span class="text-red"></span></label>
                    <div class="col-sm-1">
                        <input type="text" name="name<?php echo $count1; ?>" id="name<?php echo $count1; ?>"
                               value="{{$v->name}}" class="form-control" required>
                    </div>
                    <div id="myoptions<?php echo $count1; ?>">

                        <?php
                        $options = \App\Attributesoptions::where('product_id', $product['id'])->where('attribute_id', $v['id'])->get();
                        $count_opt = 1;
                        $option_id = "";
                        foreach ($options as $key=>$value){
                        $option_id .= $value->id . ",";
                        ?>

                        <?php
                        if ($count_opt > 1){
                        ?>

                        <div class="col-sm-12" id="option<?php echo $count1; ?>_<?php echo $count_opt; ?>">
                            <hr>
                            <?php
                            }
                            ?>

                            <div class="form-group">
                                <label class="<?php if ($count_opt != 1){?> col-md-offset-3 <?php } ?> col-sm-1 control-label">Value<span
                                            class="text-red"></span></label>
                                <div class="col-sm-1">
                                    <input type="text" name="value<?php echo $count1; ?>_<?php echo $count_opt; ?>"
                                           id="value<?php echo $count1; ?>_<?php echo $count_opt; ?>"
                                           class="form-control" value="{{$value->name}}" required>
                                </div>
                                <label class="col-sm-1 control-label">Price<span class="text-red"></span></label>
                                <div class="col-sm-1">
                                    <input type="text" name="price<?php echo $count1; ?>_<?php echo $count_opt; ?>"
                                           id="price<?php echo $count1; ?>_<?php echo $count_opt; ?>"
                                           class="form-control" value="{{$value->price}}" required>
                                </div>


                                <?php
                                if ($count_opt == 1){
                                ?>
                                <div class='col-sm-1'>
                                    <a href='javascript:;'
                                       onclick="addoption('Account1<?php echo $count1; ?>','<?php echo $count1;?>')">
                                        <strong>[+]</strong></a></div>
                                <div class="col-sm-1<?php if ($count_opt > 1) {?> col-md-offset-1 <?php } ?>">
                                    <a href="javascript:;"
                                       onclick="removeEvent1213('Account1<?php echo $count1; ?>','<?php echo $v->id;?>')">
                                        <strong>[x]</strong></a>
                                </div>
                                <?php
                                }
                                else {
                                ?>
                                <div class="col-sm-1<?php if ($count_opt>1) {?> col-md-offset-1 <?php } ?>">
                                    <a href="javascript:;"
                                       onclick="removeoption('{{$count1}}','option{{$count1}}_{{$count_opt}}','{{$value->id}}')">
                                        <strong>[x]</strong></a>
                                </div>
                                <?php
                                }
                                ?>

                            </div>

                            <?php
                            if ($count_opt > 1){
                            ?>
                        </div>
                        <?php
                        }
                        $count_opt++;
                        }
                        ?>


                    </div>
                    <input type="hidden" value="{{ trim($option_id,",") }}" name="opt_id<?php echo $count1; ?>"
                           id="opt_id<?php echo $count1; ?>">
                    <input type="hidden" id="opt_hidden<?php echo $count1; ?>" name="opt_hidden<?php echo $count1;?>"
                           value="{{$count_opt-1}}" class='form-control'>
                    <input type='hidden' id="option_remove_id<?php echo $count1; ?>"
                           name="option_remove_id<?php echo $count1; ?>" class='form-control'>
                </div>
            </div>
            <?php $count1++; ?>
        @endforeach
    @endif
</div>

<div class="form-group">
    <div class="col-sm-5">
        <input class="btn btn-info pull-right" type="button" value="Add Attribute " onClick="addEvent1213();">
    </div>
</div>


<script>

    function addEvent1213() {
        var ni = document.getElementById('myDiv1');
        var numi1 = document.getElementById('theValue1213');
        var num1 = (document.getElementById("theValue1213").value - 1) + 2;
        numi1.value = num1;

        var divIdName1 = "Account1" + num1;
        var newdiv1 = document.createElement('div');
        newdiv1.setAttribute("id", divIdName1);
        newdiv1.setAttribute("class", "col-sm-12");
        newdiv1.innerHTML = "<hr>" +
                "<div class='form-group'>" +
                "<label class='col-sm-2 control-label'>Name<span class='text-red'></span></label>" +
                "<div class='col-sm-1'><input type='text' id=\"name" + num1 + "\"  name=\"name" + num1 + "\"  class='form-control' required></div>" +
                "<label class='col-sm-1 control-label'>Value<span class='text-red'></span></label>" +
                "<div class='col-sm-1'><input type='text' id=\"value" + num1 + "_1\"  name=\"value" + num1 + "_1\"  class='form-control' required></div>" +
                "<label class='col-sm-1 control-label'>Price<span class='text-red'></span></label>" +
                "<div class='col-sm-1'><input type='text' id=\"price" + num1 + "_1\"  name=\"price" + num1 + "_1\"  class='form-control'required></div>" +
                "<div class='col-sm-1'>" +
                "<a href='javascript:;' onclick=\"addoption(\'" + divIdName1 + "\',\'" + num1 + "\')\">" +
                "<strong>[+]</strong></a>" +
                "</div>" +
                "<div class='col-sm-1'>" +
                "<a href='javascript:;' onclick=\"removeEvent1213(\'" + divIdName1 + "\')\">" +
                "<strong>[x]</strong></a>" +
                "</div>" +
                "<div id=\"myoptions" + num1 + "\"></div>" +
                "<input type='hidden' id=\"opt_hidden" + num1 + "\"  name=\"opt_hidden" + num1 + "\"  value=\"1\" class='form-control'>" +
                "<input type='hidden' id=\"option_remove_id" + num1 + "\"  name=\"option_remove_id" + num1 + "\"  class='form-control'>" +
                "</div>";
        ni.appendChild(newdiv1);
        document.getElementById("i").value = document.getElementById("i").value + 1;
    }

    function addoption(divNum1, id) {

        var ni = document.getElementById('myoptions' + id);
        var numi1 = document.getElementById('opt_hidden' + id);
        var num1 = (document.getElementById('opt_hidden' + id).value - 1) + 2;
        numi1.value = num1;

        var divIdName1 = "option" + id + "_" + num1;
        var newdiv1 = document.createElement('div');
        newdiv1.setAttribute("id", divIdName1);
        newdiv1.setAttribute("class", "col-sm-12");
        newdiv1.innerHTML = "<hr>" +
                "<div class='form-group'>" +
                "<label class='col-md-offset-3 col-sm-1 control-label'>Value<span class='text-red'></span></label>" +
                "<div class='col-sm-1'><input type='text' id=\"value" + id + "_" + num1 + "\"  name=\"value" + id + "_" + num1 + "\"  class='form-control' required></div>" +
                "<label class='col-sm-1 control-label'>Price<span class='text-red'></span></label>" +
                "<div class='col-sm-1'><input type='text' id=\"price" + id + "_" + num1 + "\"  name=\"price" + id + "_" + num1 + "\"  class='form-control' required></div>" +
                "<div class='col-sm-1 col-md-offset-1'>" +
                "<a href='javascript:;' onclick=\"removeoption(\'" + id + "\',\'" + divIdName1 + "\',\'" + num1 + "\')\">" +
                "<strong>[x]</strong></a>" +
                "</div>" +
                "</div>";
        ni.appendChild(newdiv1);
        document.getElementById("i").value = document.getElementById("i").value + 1;
    }

    function removeoption(ni, divNum1, id) {
        //alert(ni);
        // alert(divNum1);
        // alert(id);
        var remove1 = document.getElementById('option_remove_id'+ni).value;
        if (remove1 == "") {
            remove1 = id;
        }
        else {
            remove1 = remove1 + "," + id;
        }

        document.getElementById('option_remove_id'+ni).value = remove1;
        var d1 = document.getElementById("myoptions" + ni);
        var olddiv1 = document.getElementById(divNum1);
        d1.removeChild(olddiv1);
        document.getElementById("opt_hidden" + ni).value = document.getElementById("opt_hidden" + ni).value - 1;
    }


    function removeEvent1213(divNum1, id) {

        var remove1 = document.getElementById('remove_id1').value;

        if (remove1 == "") {
            if (id!=undefined){
                remove1 = id;
            }
        }
        else {
            if (id!=undefined){
                remove1 = remove1 + "," + id;
            }
            else{
                remove1 = remove1;
            }

        }




        document.getElementById('remove_id1').value = remove1;
        var d1 = document.getElementById('myDiv1');
        var olddiv1 = document.getElementById(divNum1);
        d1.removeChild(olddiv1);
        document.getElementById("theValue1213").value = document.getElementById("theValue1213").value - 1;
    }
</script>
<input type="hidden" value="{{ trim($attribute_id,",") }}" name="attribute_id" id="attribute_id">
<input type="hidden" value="" name="remove_id1" id="remove_id1">
<input type="hidden" value="{{ !empty($count1)?$count1-1:''  }}" name="theValue1213" id="theValue1213"/>
<input type="hidden" value="{{ !empty($count1)?$count1-1:''  }}" name="i" id="i"/>