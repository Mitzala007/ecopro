@extends('admin.layouts.app')
@section('content')
    <div class="content-wrapper" style="min-height: 946px;">
        <section class="content-header">
            <h1>
                <h1>
                    {{ $menu }}
                    <small>Edit</small>
                </h1>

            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('admin/product') }}">{{ $menu }}</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Product</h3>
                        </div>



                        <div class="box-body">

                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class=""><a href="{{url('admin/product/'. $product->id.'/edit')}}">General</a></li>
                                    {{--<li class=""><a href="{{url('admin/traditional/product/related/' . $product->id.'/edit')}}" >Related-Products</a></li>--}}
                                    <li ><a href="{{url('admin/product/attribute/' . $product->id.'/edit')}}">Add-Attributes</a></li>
                                    <li class="active"><a href="#addinventory" data-toggle="tab" aria-expanded="false">Attribute Stock</a></li>
                                    <li class=""><a href="{{url('admin/product/images/' . $product->id.'/edit')}}">Images</a>
                                    </li>

                                </ul>
                                <div class="tab-content" style="display: block">

                                    <div class="tab-pane active" id="addattribute">

                                        @include('admin.product.add_inventory')
                                    </div>

                                </div>
                            </div>

                        </div>
                        <div class="box-footer">
                            <a href="{{url('admin/product/category/'. $product->id.'/edit')}}">
                                <button class="btn btn-default" type="button">Back</button>
                            </a>
                            <button class="btn btn-info pull-right" type="submit">Edit</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection


