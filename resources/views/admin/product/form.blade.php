{!! Form::hidden('redirects_to', URL::previous()) !!}
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#general" data-toggle="tab" aria-expanded="true">General</a></li>
        {{--<li class=""><a href="#price" data-toggle="tab" aria-expanded="false">Prices</a></li>
        <li class=""><a href="#inventory" data-toggle="tab" aria-expanded="false">Inventory</a></li>--}}
        <li class=""><a href="#catagory" data-toggle="tab" aria-expanded="false">Categories</a></li>
        <li class=""><a href="#relatedproduct" data-toggle="tab" aria-expanded="false">Related-Products</a></li>
        <li class=""><a href="#addattribute" data-toggle="tab" aria-expanded="false">Add-Attributes</a></li>
        <li class=""><a href="#images" data-toggle="tab" aria-expanded="false">Images</a></li>
        {{--<li class=""><a href="#shipping" data-toggle="tab" aria-expanded="false">Shipping</a></li>--}}



    </ul>


    <!--General section start-->
    <div class="tab-content">
        <div class="tab-pane active" id="general">

            <div class="form-group{{ $errors->has('city_id') ? ' has-error' : '' }}">
                <label class="col-sm-1 control-label" for="city_id">City<span class="text-red">*</span></label>
                <div class="col-sm-5">
                    {!! Form::select('city_id', [''=>'Please select'] + $city, null, ['id'=>'city_id', 'class' => 'select2 form-control', 'style' => 'width: 100%']) !!}
                    @if ($errors->has('city_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('city_id') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label class="col-sm-2 control-label" for="title">Product Name<span class="text-red"> *</span></label>
                <div class="col-sm-6">
                    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Product Name']) !!}
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                <label class="col-sm-2 control-label" for="description">Description<span
                            class="text-red"></span></label>
                <div class="col-sm-6">
                    {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Description','rows'=>'10','cols'=>'80', 'id'=>'summernote']) !!}
                    @if ($errors->has('description'))
                        <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('shortdescription') ? ' has-error' : '' }}">
                <label class="col-sm-2 control-label" for="title">Short Description</label>
                <div class="col-sm-6">
                    {!! Form::text('shortdescription', null, ['class' => 'form-control', 'placeholder' => 'Product shortdescription']) !!}
                    @if ($errors->has('shortdescription'))
                        <span class="help-block">
                <strong>{{ $errors->first('shortdescription') }}</strong>
            </span>
                    @endif
                </div>
            </div>


            <div class="form-group{{ $errors->has('sku') ? ' has-error' : '' }}">
                <label class="col-sm-2 control-label" for="title">Product SKU #<span class="text-red"> *</span></label>
                <div class="col-sm-6">
                    {!! Form::text('sku', null, ['class' => 'form-control', 'placeholder' => 'Product sku']) !!}
                    @if ($errors->has('sku'))
                        <span class="help-block">
                <strong>{{ $errors->first('sku') }}</strong>
            </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('weight') ? ' has-error' : '' }}">
                <label class="col-sm-2 control-label" for="title">Weight<span class="text-red"> *</span></label>
                <div class="col-sm-6">
                    {!! Form::text('weight', null, ['class' => 'form-control', 'placeholder' => 'Product weight']) !!}
                    @if ($errors->has('weight'))
                        <span class="help-block">
                <strong>{{ $errors->first('weight') }}</strong>
            </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('shipping') ? ' has-error' : '' }}">
                <label class="col-sm-2 control-label" for="relatedproducts">Shipping Feature</label>

                <div class="col-sm-6">
                    {!! Form::select('shipping[]',\App\Tproduct::$ship, !empty($shipping_selected)?$shipping_selected:null, ['class' => 'select2 select2-hidden-accessible form-control','multiple', 'style' => 'width: 100%']) !!}

                    @if ($errors->has('shipping'))
                        <span class="help-block">
                   <strong>{{ $errors->first('shipping') }}</strong>
                        </span>
                    @endif
                </div>

            </div>


            {{--<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">--}}
            {{--<label class="col-sm-2 control-label" for="role">Status <span class="text-red"> *</span></label>--}}

            {{--<div class="col-sm-10">--}}

            {{--@foreach (\App\Tcategory::$status as $key => $value)--}}
            {{--<label>--}}
            {{--{!! Form::radio('status', $key, null, ['class' => 'flat-red']) !!} <span--}}
            {{--style="margin-right: 10px">{{ $value }}</span>--}}
            {{--</label>--}}
            {{--@endforeach--}}

            {{--@if ($errors->has('status'))--}}
            {{--<span class="help-block">--}}
            {{--<strong>{{ $errors->first('status') }}</strong>--}}
            {{--</span>--}}
            {{--@endif--}}
            {{--</div>--}}
            {{--</div>--}}

            <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                <label class="col-sm-2 control-label" for="title">Price<span class="text-red"> *</span></label>
                <div class="col-sm-6">
                    {!! Form::text('price', null, ['class' => 'form-control', 'placeholder' => 'Product price']) !!}
                    @if ($errors->has('price'))
                        <span class="help-block">
                <strong>{{ $errors->first('price') }}</strong>
            </span>
                    @endif
                </div>
            </div>
            <div class="form-group{{ $errors->has('discountprice') ? ' has-error' : '' }}">
                <label class="col-sm-2 control-label" for="title">Discount Price</label>
                <div class="col-sm-6">
                    {!! Form::text('discountprice', null, ['class' => 'form-control', 'placeholder' => 'Product discountprice']) !!}
                    @if ($errors->has('discountprice'))
                        <span class="help-block">
                <strong>{{ $errors->first('discountprice') }}</strong>
            </span>
                    @endif
                </div>
            </div>
                <div class="form-group{{ $errors->has('inventory') ? ' has-error' : '' }}">
                    <label class="col-sm-2 control-label" for="title">Inventory<span class="text-red"> *</span></label>
                    <div class="col-sm-6">
                        {!! Form::text('inventory', null, ['class' => 'form-control', 'placeholder' => 'Product inventory'])
                        !!}
                        @if ($errors->has('inventory'))
                            <span class="help-block">
                <strong>{{ $errors->first('inventory') }}</strong>
                  </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                    <label class="col-sm-2 control-label" for="status">Status<span class="text-red"> *</span></label>
                    <div class="col-sm-6">
                        {{-- {!! Form::select('stock', ['0'=>'Please
                        select']+['instock'=>'In-stock']+['outofstock'=>'Out-Of-Stock']+['preorder'=>'preorder'], null,
                        ['class' => 'select2 select2-hidden-accessible form-control', 'style' => 'width: 100%']) !!}--}}
                        {!! Form::select('status', [''=>'Please select']+ \App\Tproduct::$stock, null, ['class' => 'select2
                        select2-hidden-accessible form-control', 'style' => 'width: 100%','onchange'=>
                        'calltype(this.value);']) !!}
                        @if ($errors->has('status'))
                            <span class="help-block">
                   <strong>{{ $errors->first('status') }}</strong>
                        </span>.
                        @endif
                    </div>
                </div>


                <div class="form-group{{ $errors->has('arrivaldate') ? ' has-error' : '' }}"
                     style="@if(isset($product->status) && $product->status=="Pre-Order") visibility:visible; @else display: none;@endif" id="stock_type">
                    <label class="col-sm-2 control-label" for="title">Arrival Date</label>
                    <div class="col-sm-6">
                        {!! Form::text('arrivaldate', null, ['class' => 'form-control','placeholder' => 'Product arrivaldate']) !!}
                        @if ($errors->has('arrivaldate'))
                            <span class="help-block">
                                <strong>{{ $errors->first('arrivaldate') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="is_featured">Featured</label>

                <div class="col-sm-16">
                    <span style="margin-right: 16px"></span>     {{ Form::checkbox('is_featured', '1', null,['class' => 'flat-red' ]) }}
                </div>

            </div>


        </div>

        <!--General section End-->

        <!--Catagory section start-->

        <div class="tab-pane" id="catagory">
            @foreach($category as $cat =>$key)
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="catagory">{{$key}}</label>
                    <div class="col-sm-2">
                        {{ Form::checkbox('categoryid[]',$cat,!empty($category_selected )?$category_selected:null,['class' =>
                        'flat-red' ]) }}
                    </div>
                </div>
            @endforeach

        </div>
        <!--Catagory section End-->

        <!--Image section start-->

        <div class="tab-pane" id="images">

            <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                @include('admin.traditional.product.add_images')
            </div>

        </div>

        <!--Image section end-->

        <!--Related-Product section Start-->

        <div class="tab-pane" id="relatedproduct">

            <div class="form-group{{ $errors->has('relatedproducts') ? ' has-error' : '' }}">
                <label class="col-sm-2 control-label" for="relatedproducts">Related-Products</label>

                <div class="col-sm-6">

                    {{--{!! Form::select('relatedproducts[]', ['0'=>'Please Select']+['1'=>$rproducts], null, ['class' =>
                    'form-control', 'style' => 'width: 100%','multiple']) !!}--}}
                    {!! Form::select('relatedproducts[]',$rproducts, !empty($rproducts_selected)?$rproducts_selected:null,
                    ['class' => 'form-control', 'style' => 'height:300px','width: 100%','multiple']) !!}
                    @if ($errors->has('relatedproducts'))
                        <span class="help-block">
                   <strong>{{ $errors->first('relatedproducts') }}</strong>
                        </span>
                    @endif
                </div>

            </div>

        </div>





        <div class="tab-pane" id="addattribute">
            @include('admin.traditional.product.add_attribute')
        </div>



        <!--Related-Product section End-->
    </div>



</div>


<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('editor1');
    //bootstrap WYSIHTML5 - text editor
    $(".textarea").wysihtml5();
</script>


<script>

    $("#image").fileinput({
        showUpload: false,
        showCaption: false,
        showPreview: false,
        showRemove: false,
        browseClass: "btn btn-primary btn-lg btn_new",
    });

    function AjaxUploadImage(obj, id) {
        var file = obj.files[0];
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/jpg"];
        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
            $('#previewing' + URL).attr('src', 'noimage.png');
            alert("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
            //$("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
            return false;
        } else {
            var reader = new FileReader();
            reader.onload = imageIsLoaded;
            reader.readAsDataURL(obj.files[0]);
        }

        function imageIsLoaded(e) {

            $('#DisplayImage' + id).css("display", "block");
            $('#DisplayImage' + id).attr('src', e.target.result);
            $('#DisplayImage' + id).attr('width', '75');

        };

    }


    function calltype(val) {
        if (val == 'Pre-Order') {
            document.getElementById('stock_type').style.display = "block";
        }
        else {
            document.getElementById('stock_type').style.display = "none";
        }
    }
</script>

