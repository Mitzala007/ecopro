@extends('admin.layouts.app')

@section('content')
    <div class="content-wrapper" style="min-height: 946px;">
        <section class="content-header">
            <h1>
                <h1>
                    {{ $menu }}
                    <small>Edit</small>
                </h1>

            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('admin/group')}}"><i class="fa fa-dashboard"></i> {{ $menu }}</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit {{$menu}}</h3>
                        </div>
                        {!! Form::model($group,['url' => url('admin/group/'.$group->id),'method'=>'patch' ,'class' => 'form-horizontal','files'=>true]) !!}
                        <div class="nav-tabs-custom">
                            <div class="box-body">
                                @include ('admin.group.form')
                            </div>
                            <div class="box-footer">
                                <a href="{{ url('admin/group') }}" ><button class="btn btn-default" type="button">Back</button></a>
                                <button class="btn btn-info pull-right" type="submit">Edit</button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>


        @if($staff->count() == 0)
            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-info">
                            <div class="box-body">
                                <h4 align="center">No Data</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @else
            <section class="content">
                @include ('admin.error')
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-info">
                            <div class="box-body">
                                <table id="example2" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Edit</th>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Role</th>
                                        <th>Group</th>
                                        <th>Image</th>
                                        <th>Status</th>
                                        <th>Delete</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $count = 0;?>
                                    @foreach ($staff as $list)
                                        <?php $count++; ?>
                                        <input type="hidden" name="pid<?php echo $count;?>" id="pid<?php echo $count;?>" value="{{ $list['id'] }}" />
                                        <tr>
                                            <td>
                                                <a href="{{url('admin/staff/'.$list['id'].'/edit')}}"> <button class="btn btn-info" type="button" data-toggle="tooltip" title="Edit Staff" data-trigger="hover"><i class="fa fa-edit"></i></button></a>
                                            </td>

                                            <td>{{ $list['id'] }}</td>
                                            <td>{{ $list['name'] }}</td>
                                            <td>{{ $list['email'] }}</td>
                                            <td>{{ $list['role'] }}</td>
                                            <td>{{ $list['Group']['name'] }}</td>
                                            <td>
                                                @if($list['image']!="" && file_exists($list['image']))
                                                    <img src="{{ url($list->image) }}" width="35" height="35">
                                                @endif
                                            </td>
                                            <td>
                                                @if($list['status'] == 'active')
                                                    <div class="btn-group-horizontal" id="assign_remove_{{ $list['id'] }}" >
                                                        <button class="btn btn-success unassign ladda-button" data-style="slide-left" id="remove" ruid="{{ $list['id'] }}"  type="button"><span class="ladda-label">Active</span> </button>
                                                    </div>
                                                    <div class="btn-group-horizontal" id="assign_add_{{ $list['id'] }}"  style="display: none"  >
                                                        <button class="btn btn-danger assign ladda-button" data-style="slide-left" id="assign" uid="{{ $list['id'] }}"  type="button"><span class="ladda-label">In active</span></button>
                                                    </div>
                                                @endif
                                                @if($list['status'] == 'inactive')
                                                    <div class="btn-group-horizontal" id="assign_add_{{ $list['id'] }}"   >
                                                        <button class="btn btn-danger assign ladda-button" id="assign" data-style="slide-left" uid="{{ $list['id'] }}"  type="button"><span class="ladda-label">In active</span></button>
                                                    </div>
                                                    <div class="btn-group-horizontal" id="assign_remove_{{ $list['id'] }}" style="display: none" >
                                                        <button class="btn  btn-success unassign ladda-button" id="remove" ruid="{{ $list['id'] }}" data-style="slide-left"  type="button"><span class="ladda-label">Active</span></button>
                                                    </div>
                                                @endif
                                            </td>

                                            <td>
                                                <div class="btn-group-horizontal">
                                                    <span data-toggle="tooltip" title="Delete Staff" data-trigger="hover">
                                                        <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#myModal{{$list['id']}}"><i class="fa fa-trash"></i></button>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>

                                        <div id="myModal{{$list['id']}}" class="fade modal modal-danger" role="dialog">
                                            {{ Form::open(array('url' => 'admin/staff/'.$list['id'], 'method' => 'delete','style'=>'display:inline')) }}
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title">Delete Staff</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Are you sure you want to delete this Staff ?</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-outline">Delete</button>
                                                    </div>
                                                </div>
                                            </div>
                                            {{ Form::close() }}
                                        </div>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif
    </div>
@endsection

<script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/ladda/ladda-themeless.min.css')}}">
<script src="{{ URL::asset('assets/plugins/ladda/spin.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/ladda/ladda.min.js')}}"></script>
<script>Ladda.bind( 'input[type=submit]' );</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.assign').click(function(){
            var user_id = $(this).attr('uid');
            var l = Ladda.create(this);
            l.start();
            $.ajax({
                url: '{{url('admin/users/assign')}}',
                type: "post",
                data: {'id': user_id,'_token' : $('meta[name=_token]').attr('content')},
                success: function(data){
                    l.stop();
                    $('#assign_remove_'+user_id).show();
                    $('#assign_add_'+user_id).hide();
                }
            });
        });

        $('.unassign').click(function(){
            var user_id = $(this).attr('ruid');
            var l = Ladda.create(this);
            l.start();
            $.ajax({
                url: '{{url('admin/users/unassign')}}',
                type: "post",
                data: {'id': user_id,'_token' : $('meta[name=_token]').attr('content')},
                success: function(data){
                    l.stop();
                    $('#assign_remove_'+user_id).hide();
                    $('#assign_add_'+user_id).show();
                }
            });
        });
    });
</script>


<link rel="stylesheet" href="{{ URL::asset('assets/plugins/drag_drop/jquery-ui.css')}}">
<script src="{{ URL::asset('assets/plugins/drag_drop/jquery-1.12.4.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/drag_drop/jquery-ui.js')}}"></script>


