{!! Form::hidden('redirects_to', URL::previous()) !!}

<div class="form-group{{ $errors->has('city_id') ? ' has-error' : '' }}">
    <label class="col-sm-1 control-label" for="city_id">City<span class="text-red">*</span></label>
    <div class="col-sm-5">
        {!! Form::select('city_id', [''=>'Please select'] + $city, null, ['id'=>'city_id', 'class' => 'select2 form-control', 'style' => 'width: 100%']) !!}
        @if ($errors->has('city_id'))
            <span class="help-block">
                <strong>{{ $errors->first('city_id') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
    <label class="col-sm-1 control-label" for="category">Category <span class="text-red">*</span></label>
    <div class="col-sm-5">
        {!! Form::select('category[]', $cat_id, !empty($cat_selected)?$cat_selected:null,['class' => 'select2 select2-hidden-accessible form-control', 'style' => 'width: 100%','multiple']) !!}
        @if ($errors->has('category'))
            <span class="help-block">
                <strong>{{ $errors->first('category') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label class="col-sm-1 control-label" for="title">Name <span class="text-red">*</span></label>
    <div class="col-sm-5">
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Enter Business Name']) !!}
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
    <label class="col-sm-1 control-label" for="role">Status <span class="text-red">*</span></label>
    <div class="col-sm-5">
        @foreach (\App\Business::$status as $key => $value)
            <label>
                {!! Form::radio('status', $key, null, ['class' => 'flat-red']) !!} <span style="margin-right: 10px">{{ $value }}</span>
            </label>
        @endforeach

        @if ($errors->has('status'))
            <span class="help-block">
             <strong>{{ $errors->first('status') }}</strong>
            </span>
        @endif
    </div>
</div>

