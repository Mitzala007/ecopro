{!! Form::hidden('redirects_to', URL::previous()) !!}

<div class="form-group{{ $errors->has('parent_id') ? ' has-error' : '' }}">
    <label class="col-sm-1 control-label" for="parent_id">Parent Category</label>
    <div class="col-sm-5">
        <select name="parent_id"class="select2 form-control">
            <option value="0">Select Category</option>
            @foreach($categories as $category)
                <option value="{{ $category->id }}" @if($selected_id==$category->id) selected @endif>{{ $category->name }}</option>
                @if(count($category->childs))
                    @include('admin.category.managechild',['childs' => $category->childs])
                @endif
                </li>
            @endforeach
        </select>
        @if ($errors->has('parent_id'))
            <span class="help-block">
                <strong>{{ $errors->first('parent_id') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label class="col-sm-1 control-label" for="title">Name <span class="text-red">*</span></label>
    <div class="col-sm-5">
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Enter Name']) !!}
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
    <label class="col-sm-1 control-label" for="role">Status <span class="text-red">*</span></label>
    <div class="col-sm-5">
        @foreach (\App\Categories::$status as $key => $value)
            <label>
                {!! Form::radio('status', $key, null, ['class' => 'flat-red']) !!} <span style="margin-right: 10px">{{ $value }}</span>
            </label>
        @endforeach

        @if ($errors->has('status'))
            <span class="help-block">
             <strong>{{ $errors->first('status') }}</strong>
            </span>
        @endif
    </div>
</div>
