
@foreach($childs as $child)

	<?php
	$level = $child->level;
    $a = " ";
	for ($i=0;$i<$level;$i++){
	    $a = $a."&nbsp;&nbsp;&nbsp;";
	}
	?>
	<option value='{{ $child->id }}	' @if($selected_id==$child->id) selected @endif> {{ $a.$child->name }}	</option>
	@if(count($child->childs))
		    @include('admin.category.managechild',['childs' => $child->childs])
        @endif
@endforeach
