@foreach($childs as $child)

	<?php
	$level = $child->level;
    $a = " ";
	for ($i=0;$i<$level;$i++){
	    $a = $a."&nbsp;&nbsp;&nbsp;";
	}
	?>

	<tr class="ui-state-default" id="arrayorder_{{$child->id}}">
		<td>
			<div class="btn-group-horizontal">
				{{ Form::open(array('url' => 'admin/category/'.$child->id.'/edit', 'method' => 'get','style'=>'display:inline')) }}
				<button class="btn btn-info tip" data-toggle="tooltip" title="Edit Category" data-trigger="hover" type="submit" ><i class="fa fa-edit"></i></button>
				{{ Form::close() }}
			</div>
		</td>
		<td>{{ $child->id }}</td>
		<td> {{ $a.$child->name }}
		</td>
		<td>
			@if($list['status'] == 'active')
				<div class="btn-group-horizontal" id="assign_remove_{{ $child->id }}" >
					<button class="btn btn-success unassign ladda-button" data-style="slide-left" id="remove" ruid="{{ $child->id }}"  type="button"  style="height:28px; padding:0 12px"><span class="ladda-label">Active</span> </button>
				</div>
				<div class="btn-group-horizontal" id="assign_add_{{ $child->id }}"  style="display: none"  >
					<button class="btn btn-danger assign ladda-button" data-style="slide-left" id="assign" uid="{{ $child->id }}"  type="button"  style="height:28px; padding:0 12px" ><span class="ladda-label">In Active</span></button>
				</div>
			@endif
			@if($list['status'] == 'in-active')
				<div class="btn-group-horizontal" id="assign_add_{{$child->id }}"   >
					<button class="btn btn-danger assign ladda-button" id="assign" data-style="slide-left" uid="{{ $child->id }}"  type="button"  style="height:28px; padding:0 12px"><span class="ladda-label">In Active</span></button>
				</div>
				<div class="btn-group-horizontal" id="assign_remove_{{ $child->id}}" style="display: none" >
					<button class="btn  btn-success unassign ladda-button" id="remove" ruid="{{ $child->id }}" data-style="slide-left"  type="button"  style="height:28px; padding:0 12px"><span class="ladda-label">Active</span></button>
				</div>
			@endif
		</td>

		<td>
			<div class="btn-group-horizontal">
                <span data-toggle="tooltip" title="Delete Category" data-trigger="hover">
                    {{--<span data-toggle="modal" data-target="#myModal{{$child->id}}" style="color: #680814; font-size: 25px;"><i class="fa fa-trash"></i></span>--}}
					 <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#myModal{{$child->id}}"><i class="fa fa-trash"></i></button>
				</span>

				<div id="myModal{{$child->id}}" class="fade modal modal-danger" role="dialog">
					{{ Form::open(array('url' => 'admin/category/'.$child->id, 'method' => 'delete','style'=>'display:inline')) }}
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title">Delete Category</h4>
							</div>

							<div class="modal-body">
								<p>Are you sure you want to delete this Category ?</p>
							</div>

							<div class="modal-footer">
								<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
								<button type="submit" class="btn btn-outline">Delete</button>
							</div>
						</div>
					</div>
					{{ Form::close() }}
				</div>

			</div>
		</td>
	</tr>
	@if(count($child->childs))
		    @include('admin.category.managechild_index',['childs' => $child->childs])
        @endif
@endforeach
