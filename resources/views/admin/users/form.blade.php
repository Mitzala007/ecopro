{!! Form::hidden('redirects_to', URL::previous()) !!}

{{--<div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">--}}
    {{--<label class="col-sm-1 control-label" for="role">Role <span class="text-red">*</span></label>--}}
    {{--<div class="col-sm-5">--}}
        {{--{!! Form::select('role', \App\User::$access, !empty($role->role)?$role->role:null, ['class' => 'select2 select2-hidden-accessible form-control', 'style' => 'width: 100%']) !!}--}}
        {{--@if ($errors->has('role'))--}}
            {{--<span class="help-block">--}}
                {{--<strong>{{ $errors->first('role') }}</strong>--}}
            {{--</span>--}}
        {{--@endif--}}
    {{--</div>--}}
{{--</div>--}}

<div class="form-group{{ $errors->has('group_id') ? ' has-error' : '' }}">
    <label class="col-sm-1 control-label" for="group_id">Sub-Admin Group <span class="text-red">*</span></label>
    <div class="col-sm-5">
        {!! Form::select('group_id', [''=>'Please Select']+ $groupname, null, ['id' => 'type','class' => 'select form-control']) !!}
        @if ($errors->has('group_id'))
            <span class="help-block">
                <strong>{{ $errors->first('group_id') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
    <label class="col-sm-1 control-label" for="category">Category <span class="text-red">*</span></label>
    <div class="col-sm-5">
        {!! Form::select('category[]', $cat_id, !empty($cat_selected)?$cat_selected:null,['class' => 'select2 select2-hidden-accessible form-control', 'style' => 'width: 100%','multiple']) !!}
        @if ($errors->has('category'))
            <span class="help-block">
                <strong>{{ $errors->first('category') }}</strong>
            </span>
        @endif
    </div>
    <div class="col-md-4">
        <h5 style="color: #ff0000;"><i class="fa fa-exclamation-circle"></i> &nbsp;Select multiple categories for your products.</h5>
    </div>
</div>

<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label class="col-sm-1 control-label" for="title">Name <span class="text-red">*</span></label>
    <div class="col-sm-5">
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Enter Name']) !!}
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    <label class="col-sm-1 control-label" for="email">Email <span class="text-red">*</span></label>
    <div class="col-sm-5">
        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Enter Email Address']) !!}
            @if ($errors->has('email'))
            <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
    <label class="col-sm-1 control-label" for="password">Password <span class="text-red">*</span></label>
    <div class="col-sm-5">
        <input type="password" placeholder="Enter Password" autocomplete="off"  id="password" maxlength="10" name="password" class="form-control" >
        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
    <label class="col-sm-1 control-label" for="image">Image<span class="text-red">*</span></label>
    <div class="col-sm-5">
        <div class="">

            {!! Form::file('image', ['class' => '', 'id'=> 'image', 'onChange'=>'AjaxUploadImage(this)']) !!}
        </div>

        <?php
        if (!empty($user->image) && $user->image != "") {
        ?>
        <br><img id="DisplayImage" src="{{ url($user->image) }}" name="img" id="img" width="150" style="padding-bottom:5px" >
        <?php
        }else{
            echo '<br><img id="DisplayImage" src="" width="150" style="display: none;"/>';
        } ?>

        @if ($errors->has('image'))
            <span class="help-block">
                    <strong>{{ $errors->first('image') }}</strong>
                </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
    <label class="col-sm-1 control-label" for="role">Status <span class="text-red">*</span></label>
    <div class="col-sm-5">
        @foreach (\App\User::$status as $key => $value)
            <label>
                {!! Form::radio('status', $key, null, ['class' => 'flat-red']) !!} <span style="margin-right: 10px">{{ $value }}</span>
            </label>
        @endforeach

        @if ($errors->has('status'))
            <span class="help-block">
             <strong>{{ $errors->first('status') }}</strong>
            </span>
        @endif
    </div>
</div>


<script>

    $("#image").fileinput({
        showUpload: false,
        showCaption: false,

        showPreview: false,
        showRemove: false,
        browseClass: "btn btn-primary btn-lg btn_new",
    });

    function AjaxUploadImage(obj,id){

        var file = obj.files[0];
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/jpg"];
        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2])))
        {
            $('#previewing'+URL).attr('src', 'noimage.png');
            alert("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
            //$("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
            return false;
        } else{
            var reader = new FileReader();
            reader.onload = imageIsLoaded;
            reader.readAsDataURL(obj.files[0]);
        }
    }

    function imageIsLoaded(e) {

        $('#DisplayImage').css("display", "block");
        $('#DisplayImage').attr('src', e.target.result);
        $('#DisplayImage').attr('width', '150');

    };

</script>