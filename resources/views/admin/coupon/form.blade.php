{!! Form::hidden('redirects_to', URL::previous()) !!}

<div class="form-group{{ $errors->has('cpn_number') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="title">Coupon Code<span class="text-red">*</span></label>
    <div class="col-sm-5">
        {!! Form::text('cpn_number', null, ['class' => 'form-control', 'placeholder' => 'Title']) !!}
        @if ($errors->has('cpn_number'))
            <span class="help-block">
                <strong>{{ $errors->first('cpn_number') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="form-group{{ $errors->has('disc_type') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="role">Discount Type<span class="text-red">*</span></label>

    <div class="col-sm-5">

        @foreach (\App\Coupon::$discount_type as $key => $value)
            <label>
                {!! Form::radio('disc_type', $key, null, ['class' => 'flat-red']) !!} <span style="margin-right: 10px">{{ $value }}</span>
            </label>
        @endforeach

        @if ($errors->has('disc_type'))
            <span class="help-block">
             <strong>{{ $errors->first('disc_type') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="discountcode">Discount Amount/Percentage<span class="text-red">*</span></label>
    <div class="col-sm-5">
        {!! Form::text('amount', null, ['class' => 'form-control', 'placeholder' => 'Amount']) !!}
        @if ($errors->has('amount'))
            <span class="help-block">
                <strong>{{ $errors->first('amount') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="form-group{{ $errors->has('cupon_use') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="total_use">Cupon use<span class="text-red">*</span></label>
    <div class="col-sm-5">
        {!! Form::text('cupon_use', null, ['class' => 'form-control', 'placeholder' => 'Cupon use']) !!}
        @if ($errors->has('cupon_use'))
            <span class="help-block">
                <strong>{{ $errors->first('cupon_use') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="form-group{{ $errors->has('member_use') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="member_use">Member/per use<span class="text-red">*</span></label>
    <div class="col-sm-5">
        {!! Form::text('member_use', null, ['class' => 'form-control', 'placeholder' => 'Member use']) !!}
        @if ($errors->has('member_use'))
            <span class="help-block">
                <strong>{{ $errors->first('member_use') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="start_date">Start Date<span class="text-red">*</span></label>
    <div class="col-sm-5">
        <div class="input-group date">
            <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
            </div>
            {!! Form::text('start_date', null, ['class' => 'form-control pull-right','id'=>'datepicker','name'=>'start_date']) !!}

        </div>
        @if ($errors->has('start_date'))
            <span class="help-block">
                <strong>{{ $errors->first('start_date') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="end_date">End Date<span class="text-red">*</span></label>
    <div class="col-sm-5">
        <div class="input-group date">
            <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
            </div>
            {!! Form::text('end_date', null, ['class' => 'form-control pull-right','id'=>'datepicker1','name'=>'end_date']) !!}

        </div>
        @if ($errors->has('end_date'))
            <span class="help-block">
                <strong>{{ $errors->first('end_date') }}</strong>
            </span>
        @endif
    </div>

</div>


<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="role">Status<span class="text-red">*</span></label>

    <div class="col-sm-5">

        @foreach (\App\Coupon::$status as $key => $value)
            <label>
                {!! Form::radio('status', $key, null, ['class' => 'flat-red']) !!}<span style="margin-right: 10px">{{ $value }}</span>
            </label>
        @endforeach

        @if ($errors->has('status'))
            <span class="help-block">
             <strong>{{ $errors->first('status') }}</strong>
            </span>
        @endif
    </div>
</div>
