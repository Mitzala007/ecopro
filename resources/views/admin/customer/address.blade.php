<div id="myDiv">
    <?php  $address_id = ""; ?>
    @if(isset($customer->customeraddress[0]) && !empty($customer->customeraddress[0]))
        <?php $count = 1;
        ?>
        @foreach ($customer->customeraddress as $key => $value)
            <?php $address_id .= $value->id . ",";
            ?>

            <div class="col-sm-12" id="Account<?php echo $count; ?>">
                @if($count!=1)
                    <hr>
                @endif
                <div class="form-group">
                    <label class="col-sm-2 control-label">Address name<span class="text-red"></span></label>
                    <div class="col-sm-3">
                        <input type="text" name="addressname<?php echo $count; ?>" id="addressname<?php echo $count; ?>"
                               value="{{$value->addressname}}" class="form-control">
                    </div>

                    <label class="col-sm-2 control-label">First name<span class="text-red"></span></label>
                    <div class="col-sm-3">
                        <input type="text" name="first_name<?php echo $count; ?>" id="first_name<?php echo $count; ?>"
                               class="form-control" value="{{$value->first_name}}">
                    </div>

                    <div class="col-sm-1">
                        <a href="javascript:;"
                           onclick="removeEvent1212('Account<?php echo $count; ?>','<?php echo $value->id;?>')">
                            <strong>[x]</strong></a>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Last name<span class="text-red"></span></label>
                    <div class="col-sm-3">
                        <input type="text" name="last_name<?php echo $count; ?>" id="last_name<?php echo $count; ?>"
                               class="form-control" value="{{$value->last_name}}">
                    </div>

                    <label class="col-sm-2 control-label">Receiver phone <span class="text-red"></span></label>
                    <div class="col-sm-3">
                        <input type="text" name="phone<?php echo $count; ?>" id="phone<?php echo $count; ?>"
                               value="{{$value->phone}}" onkeyup="displayunicode(event);" class="form-control">

                    </div>
                </div>
                <!--<div class="form-group">
                    <label class="col-sm-2 control-label" >Company name<span class="text-red"></span></label>
                    <div class="col-sm-3">
                        <input type="text" name="company<?php echo $count; ?>" id="company<?php echo $count; ?>" class="form-control" value="{{$value->company}}">
                    </div>

                    <label class="col-sm-2 control-label">Region<span class="text-red"></span></label>
                    <div class="col-sm-3">
                        <select id="region<?php echo $count; ?>" name="region<?php echo $count; ?>" class="form-control">
                        <option value='')>Please select</option>
                        <option value='Hong Kong' @if($value->region == 'Hong Kong') selected @endif>香港</option>
                        <option value='Kowloon' @if($value->region == 'Kowloon') selected @endif>九龍</option>
                        <option value='New Territories' @if($value->region == 'New Territories') selected @endif> 新界 </option>
                        </select>
                    </div>
                </div>
                -->

                <div class="form-group">
                    <label class="col-sm-2 control-label">Address detail<span class="text-red"></span></label>
                    <div class="col-sm-3">
                        <textarea id="address<?php echo $count; ?>" name="address<?php echo $count; ?>"
                                  class="form-control">{{$value->address}}</textarea>
                    </div>


                </div>
            </div>

            <?php $count++; ?>
        @endforeach
    @endif
</div>
<div class="form-group">
    <div class="col-sm-5">
        <input class="btn btn-info pull-right" type="button" value="Add Address " onClick="addEvent1212();">
    </div>
</div>


<script>
    function displayunicode(e) {

        $(this).keydown(function (e) {
            var key = e.charCode || e.keyCode || 0;
            // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
            // home, end, period, and numpad decimal
            return (
            key == 8 ||
            key == 9 ||
            key == 13 ||
            key == 46 ||
            key == 110 ||
            key == 190 ||
            (key >= 35 && key <= 40) ||
            (key >= 48 && key <= 57) ||
            (key >= 96 && key <= 105));
        });

    }


    function addEvent1212() {

        var ni = document.getElementById('myDiv');
        var numi = document.getElementById('theValue1212');
        var num = (document.getElementById("theValue1212").value - 1) + 2;
        numi.value = num;

        var divIdName = "Account" + num;
        var newdiv = document.createElement('div');
        newdiv.setAttribute("id", divIdName);
        newdiv.setAttribute("class", "col-sm-12");
        newdiv.innerHTML = "<hr>" +
                "<div class='form-group'>" +
                "<label class='col-sm-2 control-label'>Address name<span class='text-red'></span></label>" +
                "<div class='col-sm-3'><input type='text' id=\"addressname" + num + "\"  name=\"addressname" + num + "\"  class='form-control'></div>" +
                "<label class='col-sm-2 control-label'>First name<span class='text-red'></span></label>" +
                "<div class='col-sm-3'><input type='text' id=\"first_name" + num + "\"  name=\"first_name" + num + "\"  class='form-control'></div>" +
                "<div class='col-sm-1'>" +
                "<a href='javascript:;' onclick=\"removeEvent1212(\'" + divIdName + "\')\">" +
                "<strong>[x]</strong></a>" +
                "</div>" +
                "</div>" +
                "<div class='form-group'>" +
                "<label class='col-sm-2 control-label'>Last name<span class='text-red'></span></label>" +
                "<div class='col-sm-3'><input type='text' id=\"last_name" + num + "\"  name=\"last_name" + num + "\"  class='form-control'></div>" +
                "<label class='col-sm-2 control-label'>Receiver phone <span class='text-red'></span></label>" +
                "<div class='col-sm-3'><input type='text' id=\"phone" + num + "\"  name=\"phone" + num + "\" class='form-control'></div>" +
                "</div>" +
//                "<div class='form-group'>" +
//                "<label class='col-sm-2 control-label'>Company name<span class='text-red'></span></label>" +
//                "<div class='col-sm-3'><input type='text' id=\"company" + num + "\"  name=\"company" + num + "\" class='form-control'></div>" +
//                "<label class='col-sm-2 control-label' >Region<span class='text-red'></span></label>" +
//                "<div class='col-sm-3'><select id=\"region" + num + "\"  name=\"region" + num + "\" class='form-control'>" +
//                   "<option value=''>Please select</option>" +
//                    "<option value='Hong Kong'>香港</option>" +
//                    "<option value='Kowloon'>九龍</option>" +
//                    "<option value='New Territories'> 新界 </option></select></div>" +
//                "</div>" +
                "<div class='form-group'>" +
                "<label class='col-sm-2 control-label'>Address detail<span class='text-red'></span></label>" +
                "<div class='col-sm-3'><textarea class='form-control' rows='3' id=\"address" + num + "\"  name=\"address" + num + "\"></textarea></div>" +

                "</div>";
        ni.appendChild(newdiv);
        document.getElementById("i").value = document.getElementById("i").value + 1;
    }

    function removeEvent1212(divNum, id) {
        var remove = document.getElementById('remove_id').value;
        if (remove == "") {
            remove = id;
        }
        else {
            remove = remove + "," + id;
        }

        document.getElementById('remove_id').value = remove;
        var d = document.getElementById('myDiv');
        var olddiv = document.getElementById(divNum);
        d.removeChild(olddiv);
        document.getElementById("theValue1212").value = document.getElementById("theValue1212").value - 1;
    }
</script>

<input type="hidden" value="{{ trim($address_id,",") }}" name="address_id" id="address_id">
<input type="hidden" value="" name="remove_id" id="remove_id">
<input type="hidden" value="{{ !empty($count)?$count-1:''  }}" name="theValue1212" id="theValue1212"/>
<input type="hidden" value="{{ !empty($count)?$count-1:''  }}" name="i" id="i"/>
