{!! Form::hidden('redirects_to', URL::previous()) !!}
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#AccountRegistration" data-toggle="tab" aria-expanded="true">Account
                Registration</a></li>
        <li class=""><a href="#Addressmanagement" data-toggle="tab" aria-expanded="false">Address management</a></li>
        
    </ul>
    <!--General section start-->
    <div class="tab-content">
        <div class="tab-pane active" id="AccountRegistration">
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label class="col-sm-2 control-label" for="email">Email<span class="text-red"> *</span></label>

                <div class="col-sm-6">
                    {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'email']) !!}
                    @if ($errors->has('email'))
                        <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label class="col-sm-2 control-label" for="inputPassword3">Password <span
                            class="text-red">*</span></label>
                <div class="col-sm-6">
                    <input type="password" placeholder="Password" id="password" name="password" class="form-control">
                    @if ($errors->has('password'))
                        <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                         </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <label class="col-sm-2 control-label" for="inputPassword3">Confirm Password <span
                            class="text-red">*</span></label>

                <div class="col-sm-6">
                    <input type="password" placeholder="Confirm password" id="password-confirm" name="password_confirmation" class="form-control">
                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
             <strong>{{ $errors->first('password_confirmation') }}</strong>
            </span>
                    @endif
                </div>
            </div>


            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                <label class="col-sm-2 control-label" for="name">Username <span class="text-red">*</span></label>
                <div class="col-sm-6">
                    {!! Form::text('username', null, ['class' => 'form-control', 'placeholder' => 'Username']) !!}
                    @if ($errors->has('username'))
                        <span class="help-block">
                <strong>{{ $errors->first('username') }}</strong>
            </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                <label class="col-sm-2 control-label" for="gender">Gender <span class="text-red">*</span></label>

                <div class="col-sm-6">

                    @foreach (\App\Customer::$gender as $key => $value)
                        <label>
                            {!! Form::radio('gender', $key, null, ['class' => 'flat-red']) !!} <span
                                    style="margin-right: 10px">{{ $value }}</span>
                        </label>
                    @endforeach

                    @if ($errors->has('gender'))
                        <span class="help-block">
             <strong>{{ $errors->first('gender') }}</strong>
            </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                <label class="col-sm-2 control-label" for="country">City<span class="text-red"> *</span></label>

                <div class="col-sm-6">
                    {!! Form::select('city',[''=>'Please select'] + $city, null, ['class' => 'select2 select2-hidden-accessible form-control', 'style' => 'width: 100%']) !!}
                    @if ($errors->has('city'))
                        <span class="help-block">
                   <strong>{{ $errors->first('city') }}</strong>
                        </span>
                    @endif
                </div>
            </div>


            <div class="form-group{{ $errors->has('birthdate') ? ' has-error' : '' }}">
                <label class="col-sm-2 control-label" for="birthdate">Birthdate<span class="text-red"> *</span></label>
                <div class="col-sm-6">
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        {!! Form::text('birthdate', null, ['class' => 'form-control pull-right','id'=>'datepicker','name'=>'birthdate']) !!}

                    </div>
                    @if ($errors->has('birthdate'))
                        <span class="help-block">
                <strong>{{ $errors->first('birthdate') }}</strong>
            </span>
                    @endif
                </div>

            </div>

            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                <label class="col-sm-2 control-label" for="phone">Phone<span class="text-red"> *</span></label>
                <div class="col-sm-6">
                    {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Phone number']) !!}
                    @if ($errors->has('phone'))
                        <span class="help-block">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
                    @endif
                </div>
            </div>


            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                <label class="col-sm-2 control-label" for="description">Description<span class="text-red"></span></label>
                <div class="col-sm-6">
                    {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => '']) !!}
                    @if ($errors->has('description'))
                        <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
                    @endif
                </div>
            </div>
            <!-- <div class="form-group">
                <label class="col-sm-2 control-label" for="comments">Comment<span class="text-red"></span></label>
                <div class="col-sm-6">
                    {!! Form::text('comment', null, ['class' => 'form-control', 'placeholder' => 'Comment']) !!}
                </div>
            </div> -->

            <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                <label class="col-sm-2 control-label" for="role">Status <span class="text-red"> *</span></label>

                <div class="col-sm-6">

                    @foreach (\App\Customer::$status as $key => $value)
                        <label>
                            {!! Form::radio('status', $key, null, ['class' => 'flat-red']) !!} <span
                                    style="margin-right: 10px">{{ $value }}</span>
                        </label>
                    @endforeach

                    @if ($errors->has('status'))
                        <span class="help-block">
             <strong>{{ $errors->first('status') }}</strong>
            </span>
                    @endif
                </div>
            </div>
        </div>

        <div class="tab-pane" id="Addressmanagement">
            @include ('admin.customer.address')
        </div>

        

    </div>
</div>

<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('editor1');
    //bootstrap WYSIHTML5 - text editor
    $(".textarea").wysihtml5();
</script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.3.2.min.js"></script>