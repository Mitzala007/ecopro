@extends('admin.layouts.app')
@section('content')

    <link href="{{ URL::asset('assets/plugins/lightbox/assets/prism.css') }}" media="all" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('assets/plugins/lightbox/dist/lity.css') }}" media="all" rel="stylesheet" type="text/css">


    <div class="content-wrapper" style="min-height: 946px;">
        <section class="content-header">
            <h1>
                <h1>
                    {{ $menu }}
                    <small>Edit</small>
                </h1>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">{{ $menu }}</a></li>
                <li class="active">edit</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Customer </h3>
                        </div>
                        <div class="pad margin no-print">
                            <div style="margin-bottom: 0!important;" class="callout callout-info">
                                <h4><i class="fa fa-info"></i> Note:</h4>
                                Leave <strong>Password</strong> and <strong>Confirm Password</strong> empty if you are not going to change the password.
                            </div>
                        </div>

                        {!! Form::model($customer,['url' => url('admin/customer/'.$customer->id), 'method' => 'patch', 'class' => 'form-horizontal','files'=>true]) !!}

                        <div class="box-body">

                            @include ('admin.customer.form')

                        </div>
                        <div class="box-footer">
                            <a href="{{ url('admin/customer') }}" ><button class="btn btn-default" type="button">Back</button></a>
                            <button class="btn btn-info pull-right" type="submit">Edit</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection


<script src="{{ URL::asset('assets/plugins/lightbox/vendor/jquery.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/lightbox/dist/lity.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/lightbox/assets/prism.js')}}"></script>