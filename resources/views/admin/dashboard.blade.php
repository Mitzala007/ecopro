<?php
$user_group = \Illuminate\Support\Facades\Auth::user()->group_id;
$group = \App\Groups::where('id', $user_group)->first();
$right=array();
if (!empty($group)) {
	$right = explode(',', $group->accessright);
}
?>

@extends('admin.layouts.app')
@section('content')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li class="active"><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">

			<?php
			$access = 'Access Subadmin Management';
			$check = in_array($access, $right);
			?>
			@if(\Illuminate\Support\Facades\Auth::user()->role == 'admin' || $check == 1)
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="info-box">
						<span class="info-box-icon bg-yellow"><i class="fa fa-user-circle-o"></i></span>
						<div class="info-box-content">
					  		<span class="info-box-text">Total Users</span>
					  		<span class="info-box-number">{{ $total_user }}</span>
						</div>
					</div>
				</div>
			@endif



            <div class="clearfix visible-sm-block"></div>

			<?php
			$access2 = 'Access Category Management';
			$check2 = in_array($access2, $right);
			?>
			@if(\Illuminate\Support\Facades\Auth::user()->role == 'admin' || $check2 == 1)
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="info-box">
						<span class="info-box-icon bg-blue"><i class="fa fa-list-alt"></i></span>
						<div class="info-box-content">
							<span class="info-box-text">Total Categories</span>
							<span class="info-box-number">{{ $total_cat}}</span>
						</div>
				  </div>
				</div>
			@endif


			<?php
			$access4 = 'Access Product Management';
			$check4 = in_array($access4, $right);
			?>
			@if(\Illuminate\Support\Facades\Auth::user()->role == 'admin' || $check4 == 1)
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="info-box">
						<span class="info-box-icon bg-purple"><i class="fa fa-product-hunt"></i></span>
						<div class="info-box-content">
							<span class="info-box-text">Total Products</span>
							<span class="info-box-number">{{ $total_product}}</span>
						</div>
					</div>
				</div>
			@endif


			<?php
			$access7 = 'Access Customer Management';
			$check7 = in_array($access7, $right);
			?>
			@if(\Illuminate\Support\Facades\Auth::user()->role == 'admin' || $check7 == 1)
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="info-box">
						<span class="info-box-icon bg-teal"><i class="fa  fa-users"></i></span>
						<div class="info-box-content">
							<span class="info-box-text">Total Customer</span>
							<span class="info-box-number">{{ $total_customer}}</span>
						</div>
					</div>
				</div>
			@endif

        </div>
		<div class="row">
			<div class="col-md-12">
			  <div class="box box-info">
				<div class="box-header with-border">
				  <h3 class="box-title">Monthly Recap Report</h3>

				  <div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
					</button>
					<div class="btn-group">
					  <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-wrench"></i></button>
					  <ul class="dropdown-menu" role="menu">
						<li><a href="#">Action</a></li>
						<li><a href="#">Another action</a></li>
						<li><a href="#">Something else here</a></li>
						<li class="divider"></li>
						<li><a href="#">Separated link</a></li>
					  </ul>
					</div>
					<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
				  </div>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
				  <div class="row">
					<div class="col-md-8">
					  <p class="text-center">
						<strong>Sales: 1 Jan, 2014 - 30 Jul, 2014</strong>
					  </p>

					  <div class="chart">
						<!-- Sales Chart Canvas -->
						<canvas id="salesChart" style="height: 180px;"></canvas>
					  </div>
					  <!-- /.chart-responsive -->
					</div>
					<!-- /.col -->
					<div class="col-md-4">
					  <p class="text-center">
						<strong>Goal Completion</strong>
					  </p>

					  <div class="progress-group">
						<span class="progress-text">Add Products to Cart</span>
						<span class="progress-number"><b>160</b>/200</span>

						<div class="progress sm">
						  <div class="progress-bar progress-bar-aqua" style="width: 80%"></div>
						</div>
					  </div>
					  <!-- /.progress-group -->
					  <div class="progress-group">
						<span class="progress-text">Complete Purchase</span>
						<span class="progress-number"><b>310</b>/400</span>

						<div class="progress sm">
						  <div class="progress-bar progress-bar-red" style="width: 80%"></div>
						</div>
					  </div>
					  <!-- /.progress-group -->
					  <div class="progress-group">
						<span class="progress-text">Visit Premium Page</span>
						<span class="progress-number"><b>480</b>/800</span>

						<div class="progress sm">
						  <div class="progress-bar progress-bar-green" style="width: 80%"></div>
						</div>
					  </div>
					  <!-- /.progress-group -->
					  <div class="progress-group">
						<span class="progress-text">Send Inquiries</span>
						<span class="progress-number"><b>250</b>/500</span>

						<div class="progress sm">
						  <div class="progress-bar progress-bar-yellow" style="width: 80%"></div>
						</div>
					  </div>
					  <!-- /.progress-group -->
					</div>
					<!-- /.col -->
				  </div>
				  <!-- /.row -->
				</div>
				<!-- ./box-body -->
				<div class="box-footer">
				  <div class="row">
					<div class="col-sm-3 col-xs-6">
					  <div class="description-block border-right">
						<span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 17%</span>
						<h5 class="description-header">$35,210.43</h5>
						<span class="description-text">TOTAL REVENUE</span>
					  </div>
					  <!-- /.description-block -->
					</div>
					<!-- /.col -->
					<div class="col-sm-3 col-xs-6">
					  <div class="description-block border-right">
						<span class="description-percentage text-yellow"><i class="fa fa-caret-left"></i> 0%</span>
						<h5 class="description-header">$10,390.90</h5>
						<span class="description-text">TOTAL COST</span>
					  </div>
					  <!-- /.description-block -->
					</div>
					<!-- /.col -->
					<div class="col-sm-3 col-xs-6">
					  <div class="description-block border-right">
						<span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 20%</span>
						<h5 class="description-header">$24,813.53</h5>
						<span class="description-text">TOTAL PROFIT</span>
					  </div>
					  <!-- /.description-block -->
					</div>
					<!-- /.col -->
					<div class="col-sm-3 col-xs-6">
					  <div class="description-block">
						<span class="description-percentage text-red"><i class="fa fa-caret-down"></i> 18%</span>
						<h5 class="description-header">1200</h5>
						<span class="description-text">GOAL COMPLETIONS</span>
					  </div>
					  <!-- /.description-block -->
					</div>
				  </div>
				  <!-- /.row -->
				</div>
				<!-- /.box-footer -->
				</div>
			  <!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
      <!-- /.row -->
	  
		<!-- Main row -->
		<div class="row">
			<!-- Left col -->
			<div class="col-md-8">
				 
				<!-- TABLE: LATEST ORDERS -->
				<div class="box box-info">
					<div class="box-header with-border">
					  <h3 class="box-title">Latest Orders</h3>

					  <div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
					  </div>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
					  <div class="table-responsive">
						<table class="table no-margin">
						  <thead>
						  <tr>
							<th>Order ID</th>
							<th>Item</th>
							<th>Status</th>
							<th>Popularity</th>
						  </tr>
						  </thead>
						  <tbody>
						  <tr>
							<td><a href="pages/examples/invoice.html">OR9842</a></td>
							<td>Call of Duty IV</td>
							<td><span class="label label-success">Shipped</span></td>
							<td>
							  <div class="sparkbar" data-color="#00a65a" data-height="20">90,80,90,-70,61,-83,63</div>
							</td>
						  </tr>
						  <tr>
							<td><a href="pages/examples/invoice.html">OR1848</a></td>
							<td>Samsung Smart TV</td>
							<td><span class="label label-warning">Pending</span></td>
							<td>
							  <div class="sparkbar" data-color="#f39c12" data-height="20">90,80,-90,70,61,-83,68</div>
							</td>
						  </tr>
						  <tr>
							<td><a href="pages/examples/invoice.html">OR7429</a></td>
							<td>iPhone 6 Plus</td>
							<td><span class="label label-danger">Delivered</span></td>
							<td>
							  <div class="sparkbar" data-color="#f56954" data-height="20">90,-80,90,70,-61,83,63</div>
							</td>
						  </tr>
						  <tr>
							<td><a href="pages/examples/invoice.html">OR7429</a></td>
							<td>Samsung Smart TV</td>
							<td><span class="label label-info">Processing</span></td>
							<td>
							  <div class="sparkbar" data-color="#00c0ef" data-height="20">90,80,-90,70,-61,83,63</div>
							</td>
						  </tr>
						  <tr>
							<td><a href="pages/examples/invoice.html">OR1848</a></td>
							<td>Samsung Smart TV</td>
							<td><span class="label label-warning">Pending</span></td>
							<td>
							  <div class="sparkbar" data-color="#f39c12" data-height="20">90,80,-90,70,61,-83,68</div>
							</td>
						  </tr>
						  <tr>
							<td><a href="pages/examples/invoice.html">OR7429</a></td>
							<td>iPhone 6 Plus</td>
							<td><span class="label label-danger">Delivered</span></td>
							<td>
							  <div class="sparkbar" data-color="#f56954" data-height="20">90,-80,90,70,-61,83,63</div>
							</td>
						  </tr>
						  <tr>
							<td><a href="pages/examples/invoice.html">OR9842</a></td>
							<td>Call of Duty IV</td>
							<td><span class="label label-success">Shipped</span></td>
							<td>
							  <div class="sparkbar" data-color="#00a65a" data-height="20">90,80,90,-70,61,-83,63</div>
							</td>
						  </tr>
						  </tbody>
						</table>
					  </div>
					  <!-- /.table-responsive -->
					</div>
					<!-- /.box-body -->
					<div class="box-footer clearfix">
					  <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a>
					  <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>
					</div>
					<!-- /.box-footer -->
				</div>
			  <!-- /.box -->			
			</div>
			<div class="col-md-4">
			  <!-- PRODUCT LIST -->
			  <div class="box box-info">
				<div class="box-header with-border">
				  <h3 class="box-title">Recently Added Products</h3>

				  <div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
					</button>
					<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
				  </div>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
				  <ul class="products-list product-list-in-box">
				  	@foreach($recent_product as $list)
						<li class="item">
						  	<div class="product-img">
						  		@if($list->Productimage->take(1)->count() > 0)
						  			@foreach($list->Productimage->take(1)  as $image)
							  			<img src="{{url($image->image)}}" alt="Product Image">

							  		@endforeach 		  				
							  	@else
							  		<img src="{{url('assets/dist/img/default-50x50.gif')}}" alt="Product Image">
						  			
						  		@endif
							  	
						  	</div>
						  	<div class="product-info">
								<a href="javascript:void(0)" class="product-title">{{$list->name}}
								 <span class="label label-warning pull-right">${{$list->price}}</span></a>
								<span class="product-description">
								 {{$list->shortdescription}}
								</span>
						  	</div>
						</li>
					@endforeach
					<!-- /.item -->
				
				  </ul>
				</div>
				<!-- /.box-body -->
				<div class="box-footer text-center">
				  <a href="{{url('admin/product')}}" class="uppercase">View All Products</a>
				</div>
				<!-- /.box-footer -->
			  </div>
			  <!-- /.box -->
			</div>
			<!-- /.col -->
       </div>
        
    </section>
</div>


@endsection

@section("jquery")
	<!-- ChartJS 1.0.1 -->
	
	<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
	<script src="{{ URL::asset('assets/dist/js/pages/dashboard2.js')}}"></script>
    <script src="{{ URL::asset('assets/plugins/chartjs/Chart.min.js')}}"></script>

@endsection