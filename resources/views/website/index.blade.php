<!DOCTYPE html>
<html lang="en">
<head>
	<title>Ecopro </title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<!-- <link rel="icon" type="image/png" href="{{ URL::asset('assets/website/images/icons/favicon.ico')}}"/> -->
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/website/vendor/bootstrap/css/bootstrap.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/website/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/website/vendor/animate/animate.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/website/vendor/select2/select2.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/website/vendor/countdowntime/flipclock.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/website/css/util.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/website/css/main.css')}}">
<!--===============================================================================================-->
</head>
<body style="background-color: #000;">
<div style="background-image: url('{{url('assets/website/images/icons/big_bg.png')}}'); background-repeat: no-repeat; background-position: center; background-size: 71%; height: 100%;">

</div>

<!--===============================================================================================-->
	<script src="{{ URL::asset('assets/website/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{ URL::asset('assets/website/vendor/bootstrap/js/popper.js')}}"></script>
	<script src="{{ URL::asset('assets/website/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{ URL::asset('assets/website/vendor/select2/select2.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{ URL::asset('assets/website/vendor/countdowntime/flipclock.min.js')}}"></script>
	<script src="{{ URL::asset('assets/website/vendor/countdowntime/moment.min.js')}}"></script>
	<script src="{{ URL::asset('assets/website/vendor/countdowntime/moment-timezone.min.js')}}"></script>
	<script src="{{ URL::asset('assets/website/vendor/countdowntime/moment-timezone-with-data.min.js')}}"></script>
	<script src="{{ URL::asset('assets/website/vendor/countdowntime/countdowntime.js')}}"></script>
	<script>
		$('.cd100').countdown100({
			/*Set Endtime here*/
			/*Endtime must be > current time*/
			endtimeYear: 0,
			endtimeMonth: 0,
			endtimeDate: 35,
			endtimeHours: 18,
			endtimeMinutes: 0,
			endtimeSeconds: 0,
			timeZone: "" 
			// ex:  timeZone: "America/New_York"
			//go to " http://momentjs.com/timezone/ " to get timezone
		});

		
	</script>
<!--===============================================================================================-->
	<script src="vendor/tilt/tilt.jquery.min.js"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>