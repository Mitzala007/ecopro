<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('category');
            $table->string('name');
            $table->string('sku');
            $table->text('description');
            $table->text('shortdescription');
            $table->double('weight');
            $table->string('status');
            $table->integer('displayorder');
            $table->double('price');
            $table->double('discountprice');
            $table->integer('inventory');
            $table->integer('arrivaldate')->nullable();
            $table->string('shipping');
            $table->integer('is_featured');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
