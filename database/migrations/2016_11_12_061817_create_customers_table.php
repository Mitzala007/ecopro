<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username');
            $table->string('email')->unique();
            $table->string('recommend_email')->nullable();
            $table->string('password',255);
            $table->string('gender');
            $table->string('city');
            $table->string('description');
            $table->date('birthdate');
            $table->string('phone');
            $table->integer('newsletter')->nullable();
            $table->integer('term_agree')->nullable();
            $table->string('status')->comment("inactive,active")->default("active");
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customers');
    }
}
