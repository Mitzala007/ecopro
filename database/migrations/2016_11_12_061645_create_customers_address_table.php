<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_address', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id');
            $table->string('addressname');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('company')->nullable();
            $table->string('region')->nullable();
            $table->text('address');
            $table->string('phone');
            $table->string('status')->comment("inactive,active")->default("active");
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customers_address');
    }
}
