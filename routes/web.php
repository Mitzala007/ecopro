<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'admin/home'], function () {

    Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
    Route::auth();
    Route::get('/', 'admin\DashboardController@index');

    Route::get('dashboard', 'admin\DashboardController@index');

    /* USER MANAGEMENT */
    Route::put('users/assign', 'admin\UserController@assign');
    Route::put('users/unassign', 'admin\UserController@unassign');
    Route::resource('users', 'admin\UserController');
    Route::resource('profile_update', 'admin\ProfileupdateController');

    /* CITY MANAGEMENT */
    Route::put('city/assign', 'admin\CityController@assign');
    Route::put('city/unassign', 'admin\CityController@unassign');
    Route::resource('city', 'admin\CityController');

    /* CATEGORY MANAGEMENT */
    Route::put('category/assign', 'admin\CategoryController@assign');
    Route::put('category/unassign', 'admin\CategoryController@unassign');
    Route::resource('category', 'admin\CategoryController');

    /* BUSINESS MANAGEMENT */
    Route::put('business/assign', 'admin\BusinessController@assign');
    Route::put('business/unassign', 'admin\BusinessController@unassign');
    Route::resource('business', 'admin\BusinessController');

    /* BANNER MANAGEMENT */
    Route::put('banner/assign', 'admin\BannerController@assign');
    Route::put('banner/unassign', 'admin\BannerController@unassign');
    Route::resource('banner', 'admin\BannerController');

    /*COUPON CODE MANAGEMENT*/
    Route::put('coupon/assign','admin\CouponController@assign');
    Route::put('coupon/unassign','admin\CouponController@unassign');
    Route::resource('coupon', 'admin\CouponController');

    /* PRODUCT MANAGEMENT */
    Route::post('product/update_display_order', 'admin\ProductController@update_display_order');
    Route::get('product/attribute/{id}/edit', 'admin\ProductController@attribute_edit');
    Route::patch('product/attribute/{id}', 'admin\ProductController@attribute_update');
    Route::get('product/inventory/{id}/edit', 'admin\ProductController@inventory_edit');
    Route::patch('product/inventory/{id}', 'admin\ProductController@inventory_update');
    Route::get('product/inventory/destroy/{pid}/{id}', 'admin\ProductController@inventory_delete');
    Route::get('product/images/{id}/edit', 'admin\ProductController@images_edit');
    Route::patch('product/images/{id}', 'admin\ProductController@images_update');
    Route::resource('product', 'admin\ProductController');
    Route::get('product/{id}/{vid}', 'admin\ProductController@update_status');

    /* SALOON-WORK MANAGEMENT */
    Route::put('saloon/work/assign', 'admin\SaloonworkController@assign');
    Route::put('saloon/work/unassign', 'admin\SaloonworkController@unassign');
    Route::resource('saloon/work', 'admin\SaloonworkController');

    /* SALOON MANAGEMENT */
    Route::put('saloon/assign', 'admin\SaloonController@assign');
    Route::put('saloon/unassign', 'admin\SaloonController@unassign');
    Route::resource('saloon', 'admin\SaloonController');

    /* CUSTOMER MANAGEMENT */
    Route::put('customer/assign', 'admin\CustomerController@assign');
    Route::put('customer/unassign', 'admin\CustomerController@unassign');
    Route::resource('customer', 'admin\CustomerController');
    Route::post('customer/{id}/save_comment', 'admin\CustomerController@save_comment');
   
//    Route::put('api/cms/customer/assign','admin\CustomerController@assign');
//    Route::put('api/cms/customer/unassign','admin\CustomerController@unassign');

    /*Group Managment*/
    Route::post('group/assign','admin\GroupController@assign');
    Route::post('group/unassign','admin\GroupController@unassign');
    Route::resource('group','admin\GroupController');

    Auth::routes();

});



/*------------------------------------ROUTE FOR WEBSITE---------------------------------*/

Route::get('/', 'website\HomeController@index');